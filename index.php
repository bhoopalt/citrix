<?php
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=dshema041.pdf');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header("Location: dshema041.pdf");
 exit;
session_start();
if(!isset($_SESSION['login_user']))
{
    header("Location: login.php?page=index.php");
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Citrix Ready Online TestKit</title>
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link type="text/css" rel="stylesheet" href="https://s3.amazonaws.com/citrix-cdn/can.cdn/marketing/assets/fonts/citrix-fonts.css"/>
    <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
</head>
<body>
    <div class="top-bar ctx_top_bg">
        <div class="top-bar-right">
            <ul class="menu ctx_menu1">
                <li><a href="#">My Account</a></li>
                <li><a href="" style="pointer-events:none;">|</a></li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </div>
    <div class="top-bar ctx_top_bg1" style="border-bottom: 2px solid #dddddd;">
        <div class="top-bar-left">
            <img src="img/ready-logo.png" alt="">
        </div>
    </div>
    <br>
    <div class="row medium-9 large-9 columns">
        <h1>Let's collect some information</h1>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. <br><br> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.
        </p>
        <br>



            <div style="border:1px solid #E6E6E6;">
                
                    <div class="row">
                        <div class="large-11 columns select_down">
                            <h4>Select Citrix product you are testing with, test kit and product type</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-2 large-offset-1 columns">
                            <label for="middle-label" class="middle" style="font-family:">Citrix Product</label>
                        </div>
                        <div class="large-8 columns float-left">
                            <select>
                                <option value="">Select an Citrix Product</option>  
                                <option value="">XenDesktop6.2</option>
                                <option value="">Small</option>
                                <option value="">Medium</option>
                                <option value="">Large</option>
                                
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-2 large-offset-1 columns">
                            <label for="middle-label" class="middle">Product Type</label>
                        </div>
                        <div class="large-8 columns float-left">
                            <select>
                                <option value="">Select an Citrix Product Type</option>
                                <option value="">Thin Clients</option>
                                <option value="">Small</option>
                                <option value="">Medium</option>
                                <option value="">Large</option>
                            </select>
                        </div>
                    </div>
                
                <div class="next_page"><a href="">Next ></a></div>

        </div>
    </div>
    <br><br><br>
    <footer>
        <div class="row expanded callout secondary">
            <div class="medium-6 columns">
            <img src="img/citrix-logo.png" alt="">
            </div>
            <div class="medium-6 columns">
            <div class="float-right">© 1999-2015 Citrix Systems, Inc. All Rights Reserved. &nbsp;&nbsp;|&nbsp;&nbsp;<a href="//www.citrix.com/about/legal.html" target="_blank">Privacy &amp; Terms</a></div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script>
      $(document).foundation();
    </script>
</body>
</html>
