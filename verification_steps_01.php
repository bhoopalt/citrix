<?php
session_start();
if(!isset($_SESSION['login_user']))
{
    header("Location: login.php?page=verification_steps_01.php");
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Citrix Ready Online TestKit</title>
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link type="text/css" rel="stylesheet" href="https://s3.amazonaws.com/citrix-cdn/can.cdn/marketing/assets/fonts/citrix-fonts.css"/>
    <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
</head>
<body>
    <div class="top-bar ctx_top_bg">
        <div class="top-bar-right">
            <ul class="menu ctx_menu1">
                <li><a href="#">My Account</a></li>
                <li><a href="" style="pointer-events:none;">|</a></li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </div>
    <div class="top-bar ctx_top_bg1">
        <div class="top-bar-left">
            <img src="img/ready-logo.png" alt="">
        </div>
    </div>

    <div style="height:40px;paddding:0px;background-color:#E6E6E6;">
        <div class="row">
            <div class="medium-12 large-12 columns txt_nab">
                <ul class="breadcrumb breadcrumb-arrow" align="center">
                    <li><a href="#" class="active">Introduction</a></li>
                    <li><a href="#">Prerequisites</a></li>
                    <li><a href="#">Verification Steps</a></li>
                    <li><a href="#">Test Results</a></li>
                    <li><a href="#">Confirmation</a></li>
                </ul>
            </div>
        </div>
    </div>
    <br>

    <div class="row medium-10 large-10 columns">
       <div class="large-2  columns float-right">
           
            <select class="top_select">
                <option value="">Scenarios</option>  
                <option value="">XenDesktop6.2</option>
                <option value="">Small</option>
                <option value="">Medium</option>
                <option value="">Large</option>
                
            </select>
        
       </div> 
    </div>

    <div class="row medium-10 large-10 columns">
        <div class="large-12 columns">
            <h3>Scenario 1 of 14</h3>
            <h1>(TC 1) Application Installation (Mandatory)</h1>
            <h4>Description:</h4>
            <p>The ISV applicatio being verified must be installed on Two servers (Windows 2008 R2 SP1 or Windows 2012/2012 R2 Server) running with XenApp 7.6 Delivery Agents (DA. To verify the success of the base install, the tester should verify that the application is functioning as expected from the local console prior to proceeding with subsequent tests using the ICA client. </p>
            <h4>Configration:</h4>
            <p>
            <ul class="inline-list list_config">
                <li>Two servers (<strong>Windows 2008 R2 SP1 or Windows 2012/2012 R2 Server</strong>) running with XenApp 7.6 <strong>Delivery Agent (DA)</strong> Installed.</li>
                <li>Published Apllication: ISV application installed media.</li>
            </ul>
            </p>
            
            <br>
            <div style="border:1px solid #E6E6E6;">
                
                <div class="row">
                    <div class="large-11 columns select_down">
                        <h4>Step 1 of 1</h4>
                    <!-- </div>
                    <div class="large-11 columns"> -->
                        <ul class="inline-list list_data">
                          <li>Install the ISV application on both servers with standard procedure.</li>
                          <li>Test that the application in functioning as expected from the local server consoles using both administrator and non-administrator users and upload screenshot.</li>
                        <br>
                        <div class="float-left upload_space"><a href="#" class="upload">Upload</a></div>
                        <br>
                        </ul>
                    </div>
                </div>
                <div class="next_page"><a href="">&nbsp;</a></div>
            </div>
            <br>

            <!-- Examples starts -->
            <div style="border:1px solid #E6E6E6;">
                <div class="row">
                    <div class="large-11 columns select_down">
                        <h4>Steps :</h4>
                        <form>
                            <div class="row">
                                <div class="medium-6 columns">
                                  <label>First Name
                                    <input type="text" placeholder="Text example">
                                  </label>
                                </div>
                                <div class="medium-6 columns">
                                  <label>Last Name
                                    <input type="text" placeholder="Text example">
                                  </label>
                                </div>
                                <div class="medium-12 columns">
                                    <label>
                                        Textarea example
                                        <textarea placeholder="Textarea example"></textarea>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <fieldset class="large-6 columns">
                                    <legend>Radio Buttons example</legend>
                                    <input type="radio" name="radio" value="Radio 1" id="radio1" required><label for="radio1">Radio 1</label>
                                    <input type="radio" name="radio" value="Radio 2" id="radio2"><label for="radio2">Radio 2</label>
                                    <input type="radio" name="radio" value="Radio 3" id="radio3"><label for="radio3">Radio 3</label>
                                </fieldset>
                                <fieldset class="large-6 columns">
                                    <legend>Checkbox example</legend>
                                    <input id="checkbox1" type="checkbox"><label for="checkbox1">Checkbox 1</label>
                                    <input id="checkbox2" type="checkbox"><label for="checkbox2">Checkbox 2</label>
                                    <input id="checkbox3" type="checkbox"><label for="checkbox3">Checkbox 3</label>
                                </fieldset>
                            </div>
                            <div class="row">
                                <div class="medium-12 columns">
                                    <label>Select Box
                                        <select>
                                            <option value="">Select one product</option>
                                            <option value="Product 1">Product 1</option>
                                            <option value="Product 2">Product 2</option>
                                            <option value="Product 3">Product 3</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="medium-12 columns">
                                    <div class="float-left"><label>File upload &nbsp;&nbsp;<a href="#" class="upload">Upload</a></label></div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="next_page"><a href="">&nbsp;</a></div>
            </div>

            <!-- Examples end -->
            
            <br><br>
            <div class="float-left"><a href="#" class="go_back">&nbsp;Pre-requisites&nbsp;</a></div>
            <div class="float-right"><a href="#" class="view_btn">&nbsp;&nbsp;&nbsp;Next&nbsp;&nbsp;&nbsp;</a></div>
        </div>
    </div>
    <br><br>

    <footer>
        <div class="row expanded callout secondary">
            <div class="medium-6 columns">
            <img src="img/citrix-logo.png" alt="">
            </div>
            <div class="medium-6 columns">
            <div class="float-right">© 1999-2015 Citrix Systems, Inc. All Rights Reserved. &nbsp;&nbsp;|&nbsp;&nbsp;<a href="//www.citrix.com/about/legal.html" target="_blank">Privacy &amp; Terms</a></div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script>
      $(document).foundation();
    </script>
</body>
</html>
