<?php 
date_default_timezone_set('asia/kolkata');
$time = date('dsmYi');
if(isset($_POST)){
	header('Content-Type: application/json');
	$data = $_POST;
	if($_FILES){
		$file = uploadFile($_FILES,$time);
		if(!empty($file)){ $data = array_merge($data,$file);}
	}
	// echo "<pre>";
	echo json_encode(array('formData'=>$data),JSON_PRETTY_PRINT);
}
function uploadFile($files,$time){

	if(is_array($files)){
		$arr = array();
		foreach($files as $key=>$value){
			if($value['error'] == 0){
				$filename = 'upload_'.$time.'_'.$value['name'];
				$filename = 'uploads/'.$filename;
				move_uploaded_file($value['tmp_name'], $filename);
				$arr[$key]=$filename;
			}
		}
		return $arr;
	}else{ return false; }
}