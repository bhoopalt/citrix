<?php
session_start();
if(!isset($_SESSION['login_user']))
{
    header("Location: login.php?page=dashboard.php");
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


    <title>Citrix Ready Online TestKit</title>
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link type="text/css" rel="stylesheet" href="https://s3.amazonaws.com/citrix-cdn/can.cdn/marketing/assets/fonts/citrix-fonts.css"/>
    <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
        <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
        <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="top-bar ctx_top_bg">
        <div class="top-bar-right">
            <ul class="menu ctx_menu1">
                <li><a href="#">My Account</a></li>
                <li><a href="" style="pointer-events:none;">|</a></li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </div>
    <div class="top-bar ctx_top_bg1" style="border-bottom: 2px solid #dddddd;">
        <div class="top-bar-left">
            <img src="img/ready-logo.png" alt="">
        </div>
    </div>
    <br>
    
    <div class="row ">
        <div class="large-12 columns pad-l-r-0">
            <div class="large-8 columns pad-l-r-0">
                <h1 style="padding-bottom: 0px;">Manage Test Kits</h1>
            </div>
            <div class="large-4 columns crt_new_right">
                <div class="float-right"><a href="#" class="view_btn">&nbsp;&nbsp;Create New&nbsp;&nbsp;</a></div>
            </div>
        </div>
    </div>
   
    <br>
    <div class="clearfix"></div>

    <div class="top-bar dashboard_nav" id="main-menu">
        <form>
            <div class="row">
                <div class="small-7 columns pad-l-r-0">
                  <div class="row">
                    <div class="small-5 columns main_colms">
                        <label class="menu_droplist">Status</label>
                        <select class="mar-space-0">
                            <option value="">All</option>  
                            <option value="">XenDesktop6.2</option>
                            <option value="">Small</option>
                            <option value="">Medium</option>
                            <option value="">Large</option>
                        </select>
                    </div>
                  
                    <div class="small-6 columns main_colms">
                        <label class="menu_droplist">Citrix&nbsp;Product</label>
                        <select class="mar-space-0">
                            <option value="">All</option>  
                            <option value="">XenDesktop6.2</option>
                            <option value="">Small</option>
                            <option value="">Medium</option>
                            <option value="">Large</option>
                        </select>
                    </div>
                  </div>
                </div>
            </div>
            <div>
                <div>
                    
                </div>
            </div>
        </form>
    </div>

    <br>
    <div class="clearfix"></div>
    <br>
    <div class="row">
        <div class="large-12 columns pad-l-r-0">
            <div class="box-body" style="display: block;">
                <div class="dataTables_wrapper" id="example_wrapper">
                    <table class="display dataTable" width="100%" >
                        <thead>
                            <tr role="row">
                                <th width="150px" class="sorting">Product Name</th>
                                <th width="150px" class="sorting">Citrix Product</th>
                                <th width="200px" class="sorting">Test Kit Name</th>
                                <th width="150px" class="sorting">Assignee</th>
                                <th width="150px" class="sorting_desc">Submitted(GMT)</th>
                                <th width="150px" class="sorting">Updated(GMT)</th>
                                <th width="50px" >Messages</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="pending">Pending</span></td>
                                <td>Xenmobile</td>
                                <td><a href="#" class="test_kit_name">citrix XenMobile 10.x Work App Verification Test Kit</a></td>
                                <td>No Assigned</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td >
                                    <a href="">
                                        <div class="mail_box_badge">
                                            <i class="fa fa-envelope-o"></i>
                                            <span class="badge">1</span>
                                        </div>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="declined">Declined</span></td>
                                <td>Xenserver</td>
                                <td><a href="#" class="test_kit_name">Citrix XenServer Hardware Test Kit for XenServer upto 6.2.x</a></td>
                                <td>No Assigned</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>
                                    <a href="">
                                        <div class="mail_box_badge">
                                            <i class="fa fa-envelope-o"></i>
                                            <span class="badge">2</span>
                                        </div>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="approve">Approved</span></td>
                                <td>XenDesktop</td>
                                <td><a href="#" class="test_kit_name">Citrix XenDesktop 7.x HDX Ready Thin Client Test Kit</a></td>
                                <td>Claire Littleton</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="pending">Pending</span></td>
                                <td>Xenmobile</td>
                                <td><a href="#" class="test_kit_name">Citrix XenServer GPU Self-Test Kit for XS upto 6.2</a></td>
                                <td>Jack Shephard</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>
                                    <a href="">
                                        <div class="mail_box_badge">
                                            <i class="fa fa-envelope-o"></i>
                                            <span class="badge">5</span>
                                        </div>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="pending">Pending</span></td>
                                <td>XenDesktop</td>
                                <td><a href="#" class="test_kit_name">citrix XenMobile 10.x Work App Verification Test Kit</a></td>
                                <td>Jack Shephard</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="declined">Declined</span></td>
                                <td>Xenserver</td>
                                <td><a href="#" class="test_kit_name">Citrix XenServer GPU Self-Test Kit for XS upto 6.2</a></td>
                                <td>Claire Littleton</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="approve">Approved</span></td>
                                <td>Xenserver</td>
                                <td><a href="#" class="test_kit_name">Citrix XenServer GPU Self-Test Kit for XS upto 6.2</a></td>
                                <td>Jack Shephard</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>
                                    <a href="">
                                        <div class="mail_box_badge">
                                            <i class="fa fa-envelope-o"></i>
                                            <span class="badge">8</span>
                                        </div>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="declined">Declined</span></td>
                                <td>Xenserver</td>
                                <td><a href="#" class="test_kit_name">citrix XenMobile 10.x Work App Verification Test Kit</a> </td>
                                <td>Jack Shephard</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="approve">Approved</span></td>
                                <td>Xenserver</td>
                                <td><a href="#" class="test_kit_name">citrix XenMobile 10.x Work App Verification Test Kit</a> </td>
                                <td>Jack Shephard</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="approve">Approved</span></td>
                                <td>Xenmobile</td>
                                <td><a href="#" class="test_kit_name">Citrix XenServer Hardware Test Kit for XenServer upto 6.2.x</a></td>
                                <td>Jack Shephard</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>
                                    <a href="">
                                        <div class="mail_box_badge">
                                            <i class="fa fa-envelope-o"></i>
                                            <span class="badge">15</span>
                                        </div>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="sorting_1">Lorem Ipsum <br><span class="approve">Approved</span></td>
                                <td>Xenserver</td>
                                <td><a href="#" class="test_kit_name">Citrix XenDesktop 7.x HDX Ready Thin Client Test Kit</a></td>
                                <td>Jack Shephard</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td>Sep. 9, 2015 8:23 AM</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <ul class="pagination float-right">
                <li class="arrow"><a href="#">Prev</a></li>
                <li class="current"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">38</a></li>
                <li><a href="#">39</a></li>
                <li class="arrow"><a href="#">Next</a></li>
            </ul>
        </div>
    </div>
    <br>


    <footer>
        <div class="row expanded callout secondary">
            <div class="medium-6 columns">
            <img src="img/citrix-logo.png" alt="">
            </div>
            <div class="medium-6 columns">
            <div class="float-right">© 1999-2015 Citrix Systems, Inc. All Rights Reserved. &nbsp;&nbsp;|&nbsp;&nbsp;<a href="//www.citrix.com/about/legal.html" target="_blank">Privacy &amp; Terms</a></div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    
    <!-- <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script> -->
    <script>
      $(document).foundation();

    </script>
</body>
</html>
