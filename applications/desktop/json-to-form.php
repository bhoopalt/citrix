<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Citrix Ready Online TestKit</title>
    <link rel="stylesheet" href="../../css/foundation.min.css">
    <link rel="stylesheet" href="../../css/custom.css">
    <link type="text/css" rel="stylesheet" href="https://s3.amazonaws.com/citrix-cdn/can.cdn/marketing/assets/fonts/citrix-fonts.css"/>
     <link rel="stylesheet" href="../../css/foundation-datepicker.css">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <!-- <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'> -->
    <style>
    hr{
        max-width:none;
    }
    </style>
</head>
<body>
    <div class="top-bar ctx_top_bg1">
        <div class="top-bar-left">
            <img src="../../img/ready-logo.png" alt="">

        </div>
    </div>
    <hr>
    <div id="main" class="expanded row">
        <div class="large-4 columns" >
            <pre id="json_code" style="overflow:hidden;">
                <form action="" id="json-input">
                    <textarea name="" id="json_input" cols="6" rows="30" ></textarea>
                    <button type="submit" class="small success button" style="position: absolute;top: 50%;left: 35%;">Render</button>
                </form>
            </pre>
        </div>
        <div class="large-1 columns">
            &nbsp;
        </div>
        <div class="large-7 columns" style="margin-top:70px">
            <div class="float-right"><a href="javascript://" onclick="$('#testkit_form').submit()" class="view_btn">&nbsp;Validate&nbsp;></a></div>
            <br><br>
            <div style="border:1px solid #E6E6E6;">
                <div class="row">
                    <div class="large-12 columns formData">
                        <form id="testkit_form"  enctype='multipart/form-data' action="json_process.php" method="post">

                        </form>
                    </div>
                </div>
                <div class="next_page"><a href="">&nbsp;</a></div>
            </div>

            <!-- Examples end -->
            
            
        </div>
    </div>
    <br><br>
    <pre>
        <div id="output">
            
        </div>
    </pre>

    <footer>
        <div class="row expanded callout secondary">
            <div class="medium-6 columns">
            <img src="../../img/citrix-logo.png" alt="">
            </div>
            <div class="medium-6 columns">
            <div class="float-right">© 1999-2015 Citrix Systems, Inc. All Rights Reserved. &nbsp;&nbsp;|&nbsp;&nbsp;<a href="//www.citrix.com/about/legal.html" target="_blank">Privacy &amp; Terms</a></div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
   <script type="text/javascript" src="../../js/json-form.js?d=123"></script>
   <script type="text/javascript" src="../../js/foundation-datepicker.js"></script>
   <script type="text/javascript">
   $(function(){
        $('#json-input').submit(function(that){
            var data = $('#json-input #json_input').val();
            $('#testkit_form').html('');
            testkitObj.collectInfo.buildForm(JSON.parse(data),'testkit_form');
            var respData = testkitObj.getCollectInfo();
            return false;
        });
         $('#testkit_form').submit(function(){
                var respData = testkitObj.getCollectInfo();
                if ( respData.hasOwnProperty('errors') ) {
                    $('#output').html(JSON.stringify(respData.errors,null,2,2));
                    console.log(respData.errors);
                } else {
                    var outputJson = respData.jsonData
                    $('#output').html(JSON.stringify(outputJson,null,2,2));
                    console.log(JSON.stringify(respData));
                }
                return false;

        });
         $('body #testkit_form').on("focus",".dt-picker",function(){
                var format = $(this).attr('data-placeholder');
                $(this).fdatepicker({
                    format:'dd-M-yyyy'
                });
            });
   });
       

   </script>
</body>
</html>
