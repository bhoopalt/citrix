<?php if(isset($_GET['file'])){
    $file = $_GET['file'].'.json';
    }else{
        $file = 'Primary Storage.json';
        } ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Citrix Ready Online TestKit</title>
    <link rel="stylesheet" href="../../css/foundation.min.css">
    <link rel="stylesheet" href="../../css/custom.css">
    <link type="text/css" rel="stylesheet" href="https://s3.amazonaws.com/citrix-cdn/can.cdn/marketing/assets/fonts/citrix-fonts.css"/>
    <link rel="stylesheet" href="../../css/foundation-datepicker.css">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'> -->
</head>
<body>
    <div class="top-bar ctx_top_bg1">
        <div class="top-bar-left">
            <img src="../../img/ready-logo.png" alt="">

        </div>
    </div>
    <hr>
     <div class="row">
        <h4>List of JSON</h4>
        <?php 
            $list = scandir('../../json');
            unset($list[0],$list[1]);
            foreach($list as $li){
         ?>
         <div class="large-6 columns" >
             <a href="javascript://" onclick="return render('<?php echo $li; ?>')"><?php echo $li; ?></a>
         </div>
         <?php } ?>
    </div>
    <hr>
    <div id="main" class="row" style="display: none">
        <div class="large-5 columns" style="border:1px solid #E6E6E6;">
            <pre id="json_code" style="overflow:auto;max-height: 1200px;">
                
            </pre>
        </div>
        <div class="large-7 columns">
            <div style="border:1px solid #E6E6E6;">
                <div class="row">
                    <div class="large-12 columns formData">

                        <form id="testkit_form"  enctype='multipart/form-data' action="json_process.php" method="post">

                        </form>
                    </div>
                </div>
                <div class="next_page"><a href="">&nbsp;</a></div>
            </div>

            <!-- Examples end -->
            
            <br><br>
            <div class="float-right"><a href="javascript://" onclick="$('#testkit_form').submit()" class="view_btn">&nbsp;Validate&nbsp;></a></div>
        </div>
    </div>
    <br><br>
    <pre>
        <div id="output">
            
        </div>
    </pre>

    <footer>
        <div class="row expanded callout secondary">
            <div class="medium-6 columns">
            <img src="../../img/citrix-logo.png" alt="">
            </div>
            <div class="medium-6 columns">
            <div class="float-right">© 1999-2015 Citrix Systems, Inc. All Rights Reserved. &nbsp;&nbsp;|&nbsp;&nbsp;<a href="//www.citrix.com/about/legal.html" target="_blank">Privacy &amp; Terms</a></div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
   <script type="text/javascript" src="../../js/test-kit.js?d=<?php echo date('sih'); ?>"></script>
   <script type="text/javascript" src="../../js/foundation-datepicker.js"></script>
   <script type="text/javascript">
        var url = "../../json/"+"<?php echo $file; ?>";
        var tagId = 'testkit_form';
        jQuery(document).ready(function() {
            /*Path of json and tagId where to populate json to form*/
            // testkitObj.collectInfo.buildForm(url,tagId,'json_code');
            $('#'+tagId).submit(function(){
                return testkitObj.formSubmit(this);
            });
            $('body #testkit_form').on("focus",".dt-picker",function(){
                var format = $(this).attr('data-placeholder');
                $(this).fdatepicker({
                    format:'dd-M-yyyy'
                });
            });

        });
          function render(url){
            $('#main').show();
            url = "../../json/"+url;
            document.getElementById(tagId).innerHTML='';
            /*@param url,fomrId, rawcodeId*/
            testkitObj.collectInfo.buildForm(url,tagId,'json_code');
        }

   </script>
</body>
</html>
