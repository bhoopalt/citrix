<?php
session_start();
if(!isset($_SESSION['login_user']))
{
    header("Location: login.php?page=welcome.php");
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Citrix Ready Online TestKit</title>
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link type="text/css" rel="stylesheet" href="https://s3.amazonaws.com/citrix-cdn/can.cdn/marketing/assets/fonts/citrix-fonts.css"/>
    <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
</head>
<body>
    <div class="top-bar ctx_top_bg">
        <div class="top-bar-right">
            <ul class="menu ctx_menu1">
                <li><a href="#">My Account</a></li>
                <li><a href="" style="pointer-events:none;">|</a></li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </div>
    <div class="top-bar ctx_top_bg1" style="border-bottom: 2px solid #dddddd;">
        <div class="top-bar-left">
            <img src="img/ready-logo.png" alt="">
        </div>
    </div>
    <br>

    <div class="row medium-10 large-10 columns">
        <div class="large-12 columns">
            <h1>Welcome</h1>
        </div>
        <div class="medium-6 columns medium-push-6">
            <a href="" target="_blank"><img class="thumbnail" src="img/750x350.png" alt=""></a>
        </div>
        <div class="medium-6 columns medium-pull-6">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti. Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. Vestibulum mollis mauris enim. Morbi euismod magna ac lorem rutrum elementum. Donec viverra auctor.</p>
            <ul class="ctx_ul_list">
                <li><a href="">Verification quidelines</a> (PDF 0.98GB)</li>
                <li><a href="">FAQ's</a></li>
            </ul>
            <div>
                <form>
                    <input id="checkbox1" type="checkbox"><label for="checkbox1">I have read <a href="">Citrix Ready test kit terms and conditions.</a></label>
                </form>
            </div>
            <br>
            <div class=""><a href="#" class="view_btn">&nbsp;&nbsp;&nbsp;&nbsp;Next&nbsp;&nbsp;&nbsp;&nbsp;</a></div>
        </div>
    </div>
    <br><br><br><br>

    <footer>
        <div class="row expanded callout secondary">
            <div class="medium-6 columns">
            <img src="img/citrix-logo.png" alt="">
            </div>
            <div class="medium-6 columns">
            <div class="float-right">© 1999-2015 Citrix Systems, Inc. All Rights Reserved. &nbsp;&nbsp;|&nbsp;&nbsp;<a href="//www.citrix.com/about/legal.html" target="_blank">Privacy &amp; Terms</a></div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script>
      $(document).foundation();
    </script>
</body>
</html>
