<?php
session_start();
if(!isset($_SESSION['login_user']))
{
    header("Location: login.php?page=verification_steps_02.php");
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Citrix Ready Online TestKit</title>
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link type="text/css" rel="stylesheet" href="https://s3.amazonaws.com/citrix-cdn/can.cdn/marketing/assets/fonts/citrix-fonts.css"/>
    <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
</head>
<body>
    <div class="top-bar ctx_top_bg">
        <div class="top-bar-right">
            <ul class="menu ctx_menu1">
                <li><a href="#">My Account</a></li>
                <li><a href="" style="pointer-events:none;">|</a></li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </div>
    <div class="top-bar ctx_top_bg1">
        <div class="top-bar-left">
            <img src="img/ready-logo.png" alt="">
        </div>
    </div>

    <div style="height:40px;paddding:0px;background-color:#E6E6E6;">
        <div class="row">
            <div class="medium-12 large-12 columns txt_nab">
                <ul class="breadcrumb breadcrumb-arrow" align="center">
                    <li><a href="#" class="active">Introduction</a></li>
                    <li><a href="#">Prerequisites</a></li>
                    <li><a href="#">Verification Steps</a></li>
                    <li><a href="#">Test Results</a></li>
                    <li><a href="#">Confirmation</a></li>
                </ul>
            </div>
        </div>
    </div>
    <br>

    <div class="row medium-10 large-10 columns">
       <div class="large-2  columns float-right">
           
            <select class="top_select">
                <option value="">Scenarios</option>  
                <option value="">XenDesktop6.2</option>
                <option value="">Small</option>
                <option value="">Medium</option>
                <option value="">Large</option>
                
            </select>
        
       </div> 
    </div>

    <div class="row medium-10 large-10 columns">
        <div class="large-12 columns">
            <h3>Scenario 6 of 14</h3>
            <h1>(TC 6) User Preferences (Optional)</h1>
            <h4>Description:</h4>
            <p>It is common that applications allow the end user to modify certain properties of the application and save these preferences for the user. Some applications that are not designed for multi-user usage will not save the user preference changes in an appropriate manner. This may cause a user to not have their changed preferences persist from one launch of the application to the next. This test verifies that the application retains user preference changes appropriately in a multi-user environment such as XenApp.</p>
            <p>Please note: If the test is not applicable for your product/solution, you can skip the test and same should be recorded.</p>
            <h4>Configration:</h4>
            <p>
            <ul class="inline-list list_config">
                <li>Workstations(s): 2 workstations with the Latest Citrix Receiver installed.</li>
                <li>Citrix XenApp: Tow servers (Windows 2008 R2 SP1 or Windows 2012 Server/2012 R2) running with XenApp 7.6 Delivery Agents(DA) Installed with Citrix Storefront configured.</li>
                <li>Profiles: Configured as Roaming</li>
            </ul>
            </p>
            
            <br>
            <div style="border:1px solid #E6E6E6;">
                
                <div class="row">
                    <div class="large-11 columns select_down">
                        <h4>Step 1 of 5</h4>
                    
                        <ul class="inline-list list_data">
                          <li>Log on ro workstation 1 using username and password for user 1.</li>
                          <li>From the workstation's Web browser, access the Citrix Storefront site.</li>
                          <li>Launch the ISV published application from Web Reciever.</li>
                          <li>Modify user preference settings associated with the application.</li>
                          <li>Note to which server the application is connected.</li>
                          <li>Exit the application (user preference changes shoud have been saved).</li>
                        </ul>

                        <form>
                          <div class="row">
                             <fieldset class="medium-12 columns ">
                                <legend>Results</legend>
                                <input type="radio" name="result" value="Passed" required><label for="">Passed</label>
                                <input type="radio" name="result" value="Failed"><label for="">Failed</label>
                                <input type="radio" name="result" value="Not Applicable"><label for="">Not Applicable</label>
                             </fieldset>
                          </div>
                    </div>
                </div>
                <div class="next_page"><a href="">&nbsp;Next >&nbsp;</a></div>
            </div>
            
            <br><br>
            <div class="float-left"><a href="#" class="go_back">&nbsp;Back&nbsp;</a></div>
            
        </div>
    </div>
    <br><br>

    <footer>
        <div class="row expanded callout secondary">
            <div class="medium-6 columns">
            <img src="img/citrix-logo.png" alt="">
            </div>
            <div class="medium-6 columns">
            <div class="float-right">© 1999-2015 Citrix Systems, Inc. All Rights Reserved. &nbsp;&nbsp;|&nbsp;&nbsp;<a href="//www.citrix.com/about/legal.html" target="_blank">Privacy &amp; Terms</a></div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script>
      $(document).foundation();
    </script>
</body>
</html>
