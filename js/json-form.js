String.prototype.htmlEncode = function(){
    return String(this)
            .replace(/"/g,'&quot;')
            .replace(/'/g,'&#39;')
            .replace(/</g,'&lt;')
            .replace(/>/g,'&gt;')
}
String.prototype.htmlDecode = function(){
    return String(this)
            .replace(/&quot;/g,'"')
            .replace(/&#39;/g,"'")
            .replace(/&lt;/g,'<')
            .replace(/&gt;/g,'>')
}
var testkitObj = testkitObj || {};
(function($, window) {

    testkitObj.ciValues = {
        formId: '',
        originalForm: {},
        error: [],
        tmp : (Math.random() / +new Date()).toString(36).replace(/[^a-z]+/g, ''),
        extraElem : []
    };

    testkitObj.collectInfo = {

        /*
         * @inputs are path of json file and tagId where to populate form
         * @returns json
         */
        buildForm: function(formJson, element) {
            testkitObj.element = document.getElementById(element);
            testkitObj.ciValues.originalForm = formJson;
            testkitObj.collectInfo.buildFormElements();

            testkitObj.ciValues.formId = element;
        },

        /*
         * getting form ajax response
         */
        buildFormElements: function() {
            var data = testkitObj.ciValues.originalForm;
            var elem = testkitObj.element;
            var title = data.title;
            var fields = data.fields;
            var h2 = document.createElement("h3");
            h2.innerHTML = title;
            elem.appendChild(h2);
            for(var i in fields){
                var groupTag = document.createElement("div");
                groupTag.setAttribute("class","group");
                var hTag = document.createElement("h4");
                var hText = document.createTextNode(fields[i]['group-title']);
                hTag.appendChild(hText);
                groupTag.appendChild(hTag);
                groupTag.appendChild(testkitObj.collectInfo.groupBlock(fields[i].items,i));
                elem.appendChild(groupTag);
                // manpulating others
                testkitObj.collectInfo.populateExtraValue();
            }
        },

        /*
         * to create the grouping of every subheading ogf the form
         */
        groupBlock: function(items,g_no) {
            var fieldset = document.createElement('div');
            fieldset.setAttribute('class','formset');
            for(var j in items){
                testkitObj.ciValues.tmp = (Math.random() / +new Date()).toString(36).replace(/[^a-z]+/g, '');
                var row = document.createElement('div');
                row.setAttribute('class','row');
                var form_group = document.createElement('div');
                form_group.setAttribute("class","form-group");
                var col4 = document.createElement("div");
                col4.setAttribute("class","medium-4 columns text-right");
                var col8_ = document.createElement("div");
                var col2 = document.createElement("div");
                col2.setAttribute("class","medium-2 columns float-right");
                var helpText = document.createElement("p");
                helpText.setAttribute("class","help-text");
                var label = document.createElement('label');
                var br = document.createElement("br");
                var lblText = document.createTextNode(items[j].label);
                labelName = items[j].label;
                helpText.setAttribute("id",labelName.replace(" ","_").toLowerCase());
                label.appendChild(lblText);
                // call findType for type of elememnt required
                var inputs = testkitObj.collectInfo.findType(items[j],g_no,j);
                var isRequire = items[j].validation.rule;
                if(isRequire.required){ var span = document.createElement("span");span.setAttribute("class","error"); var require = document.createTextNode('*');span.appendChild(require);label.appendChild(span); }
                // return;
                col4.appendChild(label);
                /* if the item has add more option  */
                col8_.appendChild(inputs);
                if(items[j].addmore){
                    if(items[j].addmore.button){
                        var btn = testkitObj.collectInfo.addButton(items[j].addmore.button,items[j].type,5,g_no,j); // addButton(label,element,limit)
                        col2.appendChild(btn);
                        form_group.appendChild(col2);
                    }
                    var nam = inputs.getAttribute("name");
                    if(nam != null){
                        inputs.setAttribute("name",nam+'[]');
                    }
                    col8_.setAttribute("class","medium-6 columns");
                }else{
                    col8_.setAttribute("class","medium-8 columns");
                }
                /* extra inputs */
                var extra = false;
                extra = items[j].extra;
                if (extra) {
                    col8_.appendChild(testkitObj.collectInfo.typeExtraText(extra, g_no, j, '',items[j]));
                    // todo
                }
                /* end extra input */
                /* end add more option */
                form_group.appendChild(col4);
                form_group.appendChild(col8_);
                form_group.appendChild(helpText);
                row.appendChild(form_group);
                fieldset.appendChild(row);
                fieldset.appendChild(br);
            }
            return fieldset;
        },
        createExtraDiv: function(text, input) {
            var div = document.createElement("div");
            var clearfix = document.createElement("div");
            clearfix.setAttribute("class", "clearfix");
            div.setAttribute("class", "col-md-12");
            var label = document.createElement("label");
            var textNode = document.createTextNode(text);
            var newId = "randomid_" + (Math.random() / +new Date()).toString(36).replace(/[^a-z]+/g, '');
            label.setAttribute("for", newId);
            label.setAttribute("class", "col-md-6");
            label.setAttribute("style","padding-bottom:5px;text-align: left;");
            input.setAttribute("id", newId);

            input.setAttribute("class", "col-md-6");
            div.appendChild(clearfix);
            label.appendChild(textNode);
            div.appendChild(label);
            div.appendChild(input);
            return div;

        },
        typeExtraText: function(formTag, g_no, i_no, date, item) {
            var date = (typeof date !== 'undefined') ? date : null;
            var item = (typeof item !== 'undefined') ? item : null;
            var lbl = formTag.label;
            var name = lbl.replace(" ", "_").toLowerCase();
            var input = document.createElement('input');
            var related = formTag.related;
            var text = formTag.hasOwnProperty("text") ? formTag.text : null;
            var placeholder = formTag.hasOwnProperty("placeholder") ? formTag.placeholder : null;
            var div = document.createElement("div");
            input.setAttribute("name", name);
            input.setAttribute("type", "text");
            input.setAttribute("data-group", g_no);
            input.setAttribute("data-item", i_no);
            input.setAttribute("data-related", related);
            input.setAttribute('disabled', true);
            if (date) input.setAttribute("class", "dt-picker");
            if (placeholder) input.setAttribute("placeholder", placeholder);
            if (placeholder) input.setAttribute("data-placeholder", placeholder);
            if(testkitObj.collectInfo.checkExtraValue(g_no,i_no,related,item)) input.removeAttribute('disabled');
            var related = item.extra.related[0];
            var vals = item.value;
            vals = vals.split(related+',');
            if(vals.length>1)  input.setAttribute('value',vals[1].htmlDecode());
            if (text) return div.appendChild(testkitObj.collectInfo.createExtraDiv(text, input));
            return div.appendChild(input);
        },
        checkExtraValue : function(j,i,name,item){
            var related = item.extra.related[0];
            var vals = item.value;
            vals = vals.split(',');
            if(testkitObj.inArray(name[0],vals)) return true;
            return false;
        },

        findType: function(types,g_no,i_no){
            var type = types.type;
            switch(type){
                case 'text':
                    return testkitObj.collectInfo.typeText(types, g_no, i_no, '');
                    break;
                case 'date':
                    return testkitObj.collectInfo.typeText(types, g_no, i_no,'date');
                    break;
                case 'select':
                    return testkitObj.collectInfo.typeSelect(types, g_no, i_no);
                    break;
                case 'multiselect':
                    return testkitObj.collectInfo.typeMultiselect(types, g_no, i_no);
                    break;
                case 'checkbox':
                    return testkitObj.collectInfo.typeCheckbox(types, g_no, i_no);
                    // return typeRadio(types);
                    break;
                case 'option':
                    return testkitObj.collectInfo.typeRadio(types, g_no, i_no);
                    break;
                case 'upload':
                    return testkitObj.collectInfo.typeUpload(types, g_no, i_no);
                    break;
                case 'file':
                    return testkitObj.collectInfo.typeUpload(types, g_no, i_no);
                    break;
                case 'textarea':
                    return testkitObj.collectInfo.typeTextarea(types, g_no, i_no);
                    // return typeText(types);
                    break;
            }
        },

        typeText: function(formTag, g_no, i_no, date) {
            var lbl = formTag.label;
            var name = lbl.replace(" ", "_").toLowerCase();
            var value = formTag.value;
            var input = document.createElement('input');
            var placeholder = formTag.hasOwnProperty("placeholder") ? formTag.placeholder : null;
            var maxlength = 200;
            if(value) value = value.htmlDecode();
            input.setAttribute("name", name);
            input.setAttribute("value", value);
            input.setAttribute("type", "text");
            input.setAttribute("data-group", g_no);
            input.setAttribute("data-item", i_no);
            if (date != "") {
                input.setAttribute("class", "dt-picker");
                maxlength = 12;
            }
            if (placeholder) {
                input.setAttribute("placeholder", placeholder);
                input.setAttribute("data-placeholder", placeholder);
            }
            input.setAttribute("maxlength", maxlength);
            testkitObj.collectInfo.validationRule(input, formTag.validation);
            return input;
        },

        typeSelect: function(formTag, g_no, i_no) {
            var lbl = formTag.label;
            var property = formTag.properties;
            var name = lbl.replace(" ","_").toLowerCase();
            var value = formTag.value;
            var key = formTag.key;
            var select = document.createElement('select');
            select.setAttribute("name",name);
            select.setAttribute("data-group",g_no);
            select.setAttribute("data-item",i_no);
            if(property){
                for(var j in property){
                    var option = document.createElement('option');
                    if(key !== undefined){
                    	if(key == j) option.setAttribute("selected",true); 
                    }else if(value){
                    	if(value == j) option.setAttribute("selected",true); 
                    }
                    option.value = j;
                    option.innerHTML = property[j];
                    select.appendChild(option);
                }
            }
            return select;
        },

        typeMultiselect : function(formTag,g_no,i_no){
            var lbl = formTag.label;
            var property = formTag.properties;
            var name = lbl.replace(" ","_").toLowerCase();
            var value = formTag.value;
            var key = formTag.key;
            value = value.split(',');
            if(key !== undefined) key = key.split(',');
            var select = document.createElement('select');
            select.setAttribute("name",name+'[]');
            select.setAttribute("multiple",true);
            select.setAttribute("data-group",g_no);
            select.setAttribute("data-item",i_no);
            if(property){
                for(var j in property){
                    var option = document.createElement('option');
                    if(key !== undefined){
                    	if(testkitObj.inArray(j,key)) option.setAttribute("selected",true);
                    }else if(value){
                    	if(testkitObj.inArray(j,value)) option.setAttribute("selected",true);
                    }
                    
                    option.value = j;
                    option.innerHTML = property[j];
                    select.appendChild(option);
                }
            }
            return select;
        },
        isExtra: function(e, that, other_value) {
            var that = that ? that : this;
            var current_value = that.getAttribute('value');
            var current_name = that.getAttribute("name");
            var elem = document.querySelectorAll("input[name='" + current_name + "'][data-related='" + current_value + "']");
            if (that.checked) {
                if (elem.length) {
                    elem[0].removeAttribute("disabled");
                    elem[0].focus();
                    if(other_value) elem[0].value = other_value.htmlDecode();
                }
            } else {
                if (elem.length) {
                    elem[0].value = "";
                    elem[0].setAttribute("disabled", true);
                }
            }
        },

        typeCheckbox : function(formTag,g_no,i_no){
            var lbl = formTag.label;
            var name = lbl.replace(" ","_").toLowerCase();
            var value = formTag.value;
            var key = formTag.key;
            value = value.split(",");
            if(key !== undefined) key = key.split(",");
            var div = document.createElement('div');
            var property = formTag.properties;
            var i = 0;
            if(property){
                for(var k in property){
                    var tmp_id = testkitObj.ciValues.tmp + i;
                    var div_ = document.createElement("div");
                    div_.setAttribute("class","medium-12 column");
                    var input = document.createElement("input");
                    var optText = document.createTextNode(property[k]);
                    var label = document.createElement("label");
                    testkitObj.collectInfo.validationRule(input,formTag.validation);
                    input.setAttribute("name",name+'[]');
                    input.setAttribute("value",k);
                    input.setAttribute("data-group",g_no);
                    input.setAttribute("data-item",i_no);
                    input.onchange = testkitObj.collectInfo.isExtra;
                    // check whether value checked or not
                    if(key !== undefined){
                    	if(testkitObj.inArray(k,key)) {
                    		input.setAttribute("checked",true);
                    		if(k == 'OTHERS'){
                    			var pos = key.indexOf(k);
                    			testkitObj.ciValues.extraElem.push({input:input,value:value[pos]});
                    		} 
                    	}
                    }else if(value){
                    	if(testkitObj.inArray(k,value)) {
                    		input.setAttribute("checked",true);
                    		if(k == 'Others' && key !== undefined){
                    			var pos = key.indexOf(k);
                    			testkitObj.ciValues.extraElem.push({input:input,value:value[pos]});
                    		} 
                    	}
                    }
                    input.setAttribute("id",tmp_id);
                    input.setAttribute("type","checkbox");
                    label.setAttribute("for",tmp_id);
                    label.appendChild(optText);
                    div_.appendChild(input);
                    div_.appendChild(label);
                    div.appendChild(div_);
                    i++;
                }
            }
            return div;
        },

        typeRadio : function(formTag,g_no,i_no){
            var lbl = formTag.label;
            var name = lbl.replace(" ","_").toLowerCase();
            var value = formTag.value;
            var key = formTag.key;
            var div = document.createElement('div');
            var property = formTag.properties;
            if(property){
            	var i =0;
                for(var k in property){
            		var tmp_id = testkitObj.ciValues.tmp + i;
                    var div_ = document.createElement("div");
                    div_.setAttribute("class","medium-6 column");
                    var input = document.createElement("input");
                    var optText = document.createTextNode(property[k]);
                    var label = document.createElement("label");
                    testkitObj.collectInfo.validationRule(input,formTag.validation);
                    input.setAttribute("name",name);
                    input.setAttribute("value",k);
                    input.setAttribute("id",tmp_id);
                    input.setAttribute("data-group",g_no);
                    input.setAttribute("data-item",i_no);
                    if(key !== undefined){
                    	if(k == key) input.setAttribute("checked",true);
                    }else if(value){
                    	if(k == value) input.setAttribute("checked",true);
                    }
                    input.setAttribute("type","radio");
                    label.setAttribute("for",tmp_id);
                    label.appendChild(optText);
                    div_.appendChild(input);
                    div_.appendChild(label);
                    div.appendChild(div_);
                    i++;
                }
            }
            return div;
        },

        typeUpload : function(formTag,g_no,i_no){
            var lbl = formTag.label;
            var name = lbl.replace(" ","_").toLowerCase();
            var value = formTag.value;
            var file = document.createElement('input');
            var div = document.createElement("div");
            var label = document.createElement("label");
            label.setAttribute("class","hollow button");
            label.setAttribute("for",lbl);
            label.innerHTML="Upload";
            file.setAttribute("name",name);
            file.setAttribute("type",'file');
            file.setAttribute("data-group",g_no);
            file.setAttribute("data-item",i_no);
            file.setAttribute("class","show-for-sr");
            file.setAttribute("id",lbl);
            testkitObj.collectInfo.validationRule(file,formTag.validation);
            div.appendChild(label);
            div.appendChild(file);
            return div;
        },

        typeTextarea : function(formTag,g_no,i_no){
            var lbl = formTag.label;
            var name = lbl.replace(" ","_").toLowerCase();
            var value = formTag.value;
            var textarea = document.createElement('textarea');
            textarea.setAttribute("name",name);
            //textarea.setAttribute("value",value);
            textarea.setAttribute("cols",3);
            textarea.setAttribute("rows",3);
            textarea.setAttribute("data-group",g_no);
            textarea.setAttribute("data-item",i_no);
            if ( (formTag.validation.rule).hasOwnProperty('required') ) {
                textarea.setAttribute("data-required", "1");
            } else {
                textarea.setAttribute("data-required", "0");
            }
            textarea.innerHTML=value;
            testkitObj.collectInfo.validationRule(textarea,formTag.validation);
            return textarea;
        },

        validationRule : function(that,validation){
            if(validation.length != 0){
                if(validation.rule){
                    var rules = validation.rule;
                    // that.setAttribute("data-rule",rules);
                }
                if(validation['error-message']){
                    var msg = validation['error-message'];
                    that.setAttribute("data-error",msg);
                }
            }
        },

        addButton : function(label,element,limit,g_no,i_no){
            var btn = document.createElement("button");
            btn.setAttribute("type","button");
            btn.setAttribute("onclick","testkitObj.collectInfo.addElement(this)");
            btn.setAttribute("data-limit",limit);
            btn.setAttribute("data-group",g_no);
            btn.setAttribute("data-item",i_no);
            btn.setAttribute("data-element",element);
            btn.setAttribute("data-label",label);
            btn.setAttribute("class","hollow button");
            btn.innerHTML = label;
            return btn;
        },

        addElement : function(that){
            element = that.getAttribute('data-element');
            limit = that.getAttribute('data-limit');
            g_no = that.getAttribute('data-group');
            i_no = that.getAttribute('data-item');
            lbl = that.getAttribute('data-label');
            var col6 = document.createElement("div");
            col6.setAttribute("class","medium-6 column");
            var col4 = document.createElement("div");
            col4.setAttribute("class","medium-4 column");
            var col2 = document.createElement("div");
            col2.setAttribute("class","medium-2 column float-right");
            var form_group = document.createElement("div");
            form_group.setAttribute("class","form-group");
            var clear = document.createElement("div");
            clear.setAttribute("style","clear:both");
            var item = {label:lbl,value:"",validation:{},type:element};
            var input = testkitObj.collectInfo.findType(item,g_no,i_no);
            var nam = input.getAttribute("name");
            input.setAttribute("name",nam+'[]');
            rm_btn = testkitObj.collectInfo.removeButton(form_group);
            col6.appendChild(input);
            col4.innerHTML="&nbsp;";
            col2.appendChild(rm_btn);
            form_group.appendChild(clear);
            form_group.appendChild(col4);
            form_group.appendChild(col6);
            form_group.appendChild(col2);
            var parent = that.parentElement.parentElement.parentElement;
            parent.appendChild(form_group);
        },

        removeButton : function(form_group){
            button =  document.createElement("button");
            button.setAttribute("class","alert hollow button");
            button.setAttribute("type","button");
            button.innerHTML = "remove";
            button.onclick = function(){ form_group.remove(); };
            return button;
        },

        populateExtraValue : function(){
        	var extraCheck = testkitObj.ciValues.extraElem;
        	if(extraCheck.length){
        		$.each(extraCheck,function(v,k){
        			var _input = k.input;
        			var _value = $(_input).val();
        			var _g_no = $(_input).attr('data-group');
        			var _item_no = $(_input).attr('data-item');
        			var _elem = document.querySelectorAll("input[value='" + _value + "'][data-group='" + _g_no + "'][data-item='" + _item_no + "']");
        			$.each(_elem,function(_k,_v){
        				testkitObj.collectInfo.isExtra(null,_v,k.value);
        			});
        		});
        	}

        }
    };

    testkitObj.inArray = function(needle, haystack){
        var length = haystack.length;
        for(var i = 0; i < length; i++) {
            if(haystack[i] == needle) return true;
        }
        return false;
    };

    testkitObj.getCollectInfo = function() {
        var id = testkitObj.ciValues.formId;
        testkitObj.ciValues.error = [];
        var values = [];
        var keys = [];
        // count the number ogf groups
        count = testkitObj.ciValues.originalForm.fields.length;
        // intialize groups in array
        for(var j=0; j<count;j++){
            values[j]=[];
            keys[j] = [];
        }
        var lists = $('#'+id+' input,#'+id+' select,#'+id+' textarea');
        for(var i=0; i<lists.length;i++){
            elem =lists[i];
            var grp = $(elem).attr('data-group');
            var item = $(elem).attr('data-item');
            var val ='';
            var key = "";
            if($(elem).is(':checkbox') || $(elem).is(':radio')){
                if($(elem).is(':checked')){
                	var temp_id = $(elem).attr('id');
                	if($(elem).val() != 'OTHERS') key = $('label[for='+temp_id+']').text();
                    val = $(elem).val();
                }
            }
            else{
            	if($(elem).attr('data-related') == 'OTHERS'){

                    key = $(elem).val();
                    key = key.htmlEncode();
                } 
                else val = $(elem).val();
            }
            // load key from selected options
            if($(elem).is('select')){ 
            	if($(elem).val()){
            		key = $(elem).find('option:selected').text();
            	} 
            }
            // loading answers to variable
            if(val || values[grp][item]){
                if(values[grp][item] && val){
                    values[grp][item]=values[grp][item]+','+val;
                }
                else if(values[grp][item] && !val){
                    values[grp][item]= values[grp][item];val.toString();
                }
                else{
                    values[grp][item]= val.toString();
                }
            }
            else {
                if(val != null)	values[grp][item]=val.toString();
                else values[grp][item] = '';
            }

            // loading text to keys

            // loading answers to variable
            if(key || keys[grp][item]){
                if(keys[grp][item] && key){
                    keys[grp][item]=keys[grp][item]+','+key;
                }
                else if(keys[grp][item] && !key){
                    keys[grp][item]= keys[grp][item];key.toString();
                }
                else{
                    keys[grp][item]= key.toString();
                }
            }
            else {
                if(key != null)	keys[grp][item]=key.toString();
                else keys[grp][item] = '';
            }
        }
        for(var k in values){
            if(values[k].length>0){
                for( var l in values[k]){
                    /*
                     *	check validation by passing
                     *	item, group position, item position,value
                     */
                     var _type = testkitObj.ciValues.originalForm.fields[k].items[l].type;
                     if(_type=='checkbox' || _type == 'option' || _type =='select' || _type=='multiselect' ){
                    	testkitObj.ciValues.originalForm.fields[k].items[l].key = values[k][l];
                    	testkitObj.ciValues.originalForm.fields[k].items[l].value = keys[k][l];
                     }else if(_type == 'text'){
                         var tmp_value = values[k][l];
                         tmp_value = tmp_value.htmlEncode();
                        testkitObj.ciValues.originalForm.fields[k].items[l].value = tmp_value;
                     }
                     else{
                    	testkitObj.ciValues.originalForm.fields[k].items[l].value = values[k][l];
                     }
                    testkitObj.validation.run(testkitObj.ciValues.originalForm.fields[k].items[l], k, l, values[k][l]);
                    testkitObj.checkExtraField(testkitObj.ciValues.originalForm.fields[k].items[l], k, l, values[k][l]);
                }
            }
        }

        var response = {};

        //$('#collectInfoCont').html(JSON.stringify(testkitObj.ciValues.originalForm,null,2));
        if ( Object.keys(testkitObj.ciValues.error).length > 0 ) {
            //$('#testCollectOut').html(JSON.stringify(testkitObj.ciValues.error,null,2));
            response.errors = testkitObj.ciValues.error;
        } else {
            //$('#testCollectOut').html(JSON.stringify(testkitObj.ciValues.originalForm,null,2));
            response.jsonData = testkitObj.ciValues.originalForm;
        }
        return response;
    };

    testkitObj.checkExtraField = function(list, group, item, value) {

        var values = list.value;
        values = values.split(',');
        if (values.length > 0) {
            for (var n in values) {
                var name = values[n];
                if(name) name = name.replace(/[^a-zA-Z0-9]/g, '');
                var fieldName = list.label;
                fieldName = fieldName.replace(" ", "_");
                fieldName = fieldName.toLowerCase();
                var elem = document.querySelectorAll("input[name^='" + fieldName + "'][data-related='" + name + "']");
                if (elem.length > 0) {
                    if (!elem[0].value) {
                        testkitObj.ciValues.error.push(list.extra.required);
                    }

                }
            }
        }

    };

    testkitObj.validation = {
        run: function(item,g_no,i_no){
            var rule = item.validation.rule;
            if(rule && Object.keys(rule).length > 0){
                for(var i in rule){
                    var err = testkitObj.validation.findValidate(i,item.value);
                    if(err){ testkitObj.ciValues.error.push(rule[i]);break;}
                }
            }
        },

        findValidate: function(rule,val){
            switch(rule){
                case 'required':
                    return testkitObj.validation.validRequired(rule,val);
                    break;

                case 'number':
                    return testkitObj.validation.validNumber(rule,val);
                    break;

                case 'email':
                    return testkitObj.validation.validEmail(rule,val);
                    break;

                default:
                    return testkitObj.validation.validDefault(rule,val);
                    break;
            }
        },

        validRequired: function(rule,val){
            if(!val.trim()){	return true;}else{return false;}
        },

        validNumber: function(rule,val){
            if(isNaN(val)){ return true; }else{ return false; }
        },

        validEmail: function(rule,val){
            var regexp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!val.match(regexp)){ return true; }else{ return false; }
        },

        validDefault: function(rule,val){
            var rule1 = rule.split('custom_');
            return eval(rule1[1])(rule,val);
        }
    };

})(jQuery, window);