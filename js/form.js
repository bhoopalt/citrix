var originalForm = '';
var error = [];
/* Object.key(obj).length for count length of Object array for manul*/
if (!Object.keys) {
    Object.keys = function (obj) {
        var arr = [],
            key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                arr.push(key);
            }
        }
        return arr;
    };
}
/* End length of object array */
function buildForm(data,element){
	var elem = document.getElementById(element);
	var title = data.title;
	var fields = data.fields;
	var h2 = document.createElement("h3");
	h2.innerHTML = title;
	elem.appendChild(h2);
	for(var i in fields){
		var groupTag = document.createElement("div");
			groupTag.setAttribute("class","group");
		var hTag = document.createElement("h4");
		var hText = document.createTextNode(fields[i]['group-title']);
			hTag.appendChild(hText);
		groupTag.appendChild(hTag);
		groupTag.appendChild(groupBlock(fields[i].items,i));
		elem.appendChild(groupTag);
	}
}
function groupBlock(items,g_no) {
		var fieldset = document.createElement('div');
			fieldset.setAttribute('class','formset');
	for(var j in items){
		var row = document.createElement('div');
			row.setAttribute('class','row');
		var form_group = document.createElement('div');
		form_group.setAttribute("class","form-group");
		var col4 = document.createElement("div");
			col4.setAttribute("class","medium-4 columns text-right");
		var col8_ = document.createElement("div");
			
		var col2 = document.createElement("div");
			col2.setAttribute("class","medium-2 columns float-right");
		var helpText = document.createElement("p");
			helpText.setAttribute("class","help-text");
		var label = document.createElement('label');
		var br = document.createElement("br");
		var lblText = document.createTextNode(items[j].label);
		labelName = items[j].label;
		helpText.setAttribute("id",labelName.replace(" ","_").toLowerCase());
		label.appendChild(lblText);
		var inputs = findType(items[j],g_no,j);
		var isRequire = items[j].validation.rule;
		if(isRequire.required){ var span = document.createElement("span");span.setAttribute("class","error"); var require = document.createTextNode('*');span.appendChild(require);label.appendChild(span); }
		// console.log(inputs);
		// return;
		col4.appendChild(label);
		/* if the item has add more option  */
		col8_.appendChild(inputs);
		if(items[j].addmore){
			if(items[j].addmore.button){
				var btn = addButton(items[j].addmore.button,items[j].type,5,g_no,j); // addButton(label,element,limit)
				col2.appendChild(btn);
				form_group.appendChild(col2);
			}
			var nam = inputs.getAttribute("name");
			if(nam != null){
				inputs.setAttribute("name",nam+'[]');
			}
			col8_.setAttribute("class","medium-6 columns");
		}else{
			col8_.setAttribute("class","medium-8 columns");
		}
		/* end add more option */
		form_group.appendChild(col4);
		form_group.appendChild(col8_);
		form_group.appendChild(helpText);
		row.appendChild(form_group);
		fieldset.appendChild(row);
		fieldset.appendChild(br);
	}
	return fieldset;
}
function findType(types,g_no,i_no){
	var type = types.type;
	switch(type){
		case 'text':
			// 
			return typeText(types,g_no,i_no);
			break;
		case 'select':
			return typeSelect(types,g_no,i_no);
			break;
		case 'multiselect':
			return typeMultiselect(types,g_no,i_no);
			break;
		case 'checkbox':
			return typeCheckbox(types,g_no,i_no);
			// return typeRadio(types);
			break;
		case 'option':
			return typeRadio(types,g_no,i_no);
			break;
		case 'upload':
			return typeUpload(types,g_no,i_no);
			break;
		case 'file':
			return typeUpload(types,g_no,i_no);
			break;
		case 'textarea':
			return typeTextarea(types,g_no,i_no);
			// return typeText(types);
			break;
	}
}
function addButton(label,element,limit,g_no,i_no){
	var btn = document.createElement("button");
	btn.setAttribute("type","button");
	btn.setAttribute("onclick","addElement(this)");
	btn.setAttribute("data-limit",limit);
	btn.setAttribute("data-group",g_no);
	btn.setAttribute("data-item",i_no);
	btn.setAttribute("data-element",element);
	btn.setAttribute("data-label",label);
	btn.setAttribute("class","hollow button");
	btn.innerHTML = label;
	return btn;
}
function typeText(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = lbl.replace(" ","_").toLowerCase();
	var value = formTag.value;
	var input = document.createElement('input');
	input.setAttribute("name",name);
	input.setAttribute("value",value);
	input.setAttribute("type","text");
	input.setAttribute("data-group",g_no);
	input.setAttribute("data-item",i_no);
	validationRule(input,formTag.validation);
	return input;
}
function typeTextarea(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = lbl.replace(" ","_").toLowerCase();
	var value = formTag.value;
	var textarea = document.createElement('textarea');
	textarea.setAttribute("name",name);
	textarea.setAttribute("value",value);
	textarea.setAttribute("cols",3);
	textarea.setAttribute("rows",3);
	textarea.setAttribute("data-group",g_no);
	textarea.setAttribute("data-item",i_no);
	textarea.innerHTML=value;
	validationRule(textarea,formTag.validation);
	return textarea;
}
function typeSelect(formTag,g_no,i_no){

	var lbl = formTag.label;
	var property = formTag.properties;
	var name = lbl.replace(" ","_").toLowerCase();
	var value = formTag.value;
	var select = document.createElement('select');
		select.setAttribute("name",name);
		select.setAttribute("data-group",g_no);
		select.setAttribute("data-item",i_no);
	if(property){
		for(var j in property){
			var option = document.createElement('option');
			if(value == j) option.setAttribute("selected",true);
			option.value = j;
			option.innerHTML = property[j]; 
			select.appendChild(option);
		}
	}
	return select;
}

function typeMultiselect(formTag,g_no,i_no){
	var lbl = formTag.label;
	var property = formTag.properties;
	var name = lbl.replace(" ","_").toLowerCase();
	var value = formTag.value;
	value = value.split(',');
	var select = document.createElement('select');
		select.setAttribute("name",name+'[]');
		select.setAttribute("multiple",true);
		select.setAttribute("data-group",g_no);
		select.setAttribute("data-item",i_no);
	if(property){
		for(var j in property){
			var option = document.createElement('option');
			if(inArray(j,value)) option.setAttribute("selected",true);
			option.value = j;
			option.innerHTML = property[j]; 
			select.appendChild(option);
		}
	}
	return select;
}

function typeCheckbox(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = lbl.replace(" ","_").toLowerCase();
	var value = formTag.value;
		value = value.split(",");
	var div = document.createElement('div');
	var property = formTag.properties;
	if(property){
		for(var k in property){
			var div_ = document.createElement("div");
			div_.setAttribute("class","medium-6 column");
			var input = document.createElement("input");
			var optText = document.createTextNode(property[k]);
			var label = document.createElement("label");
			validationRule(input,formTag.validation);
			input.setAttribute("name",name+'[]');
			input.setAttribute("value",k);
			input.setAttribute("data-group",g_no);
			input.setAttribute("data-item",i_no);
			// check whether value checked or not
			if(inArray(k,value)) input.setAttribute("checked",true);
			input.setAttribute("id",k);
			input.setAttribute("type","checkbox");
			label.setAttribute("for",k);
			label.appendChild(optText);
			div_.appendChild(input);
			div_.appendChild(label);
			div.appendChild(div_);
		}
	}
	return div;
}
function typeRadio(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = lbl.replace(" ","_").toLowerCase();
	var value = formTag.value;
	var div = document.createElement('div');
	var property = formTag.properties;
	if(property){
		for(var k in property){
			var div_ = document.createElement("div");
			div_.setAttribute("class","medium-6 column");
			var input = document.createElement("input");
			var optText = document.createTextNode(property[k]);
			var label = document.createElement("label");
			validationRule(input,formTag.validation);
			input.setAttribute("name",name);
			input.setAttribute("value",k);
			input.setAttribute("id",k);
			input.setAttribute("data-group",g_no);
			input.setAttribute("data-item",i_no);
			if(k == value) input.setAttribute("checked",true);
			input.setAttribute("type","radio");
			label.setAttribute("for",k);
			label.appendChild(optText);
			div_.appendChild(input);
			div_.appendChild(label);
			div.appendChild(div_);
		}
	}
	return div;
}
function typeUpload(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = lbl.replace(" ","_").toLowerCase();
	var value = formTag.value;
	var file = document.createElement('input');
	var div = document.createElement("div");
	var label = document.createElement("label");
		label.setAttribute("class","hollow button");
		label.setAttribute("for",lbl);
		label.innerHTML="Upload";
	file.setAttribute("name",name);
	file.setAttribute("type",'file');
	file.setAttribute("data-group",g_no);
	file.setAttribute("data-item",i_no);
	file.setAttribute("class","show-for-sr");
	file.setAttribute("id",lbl);
	validationRule(file,formTag.validation);
	div.appendChild(label);
	div.appendChild(file);
	return div;
}
function validationRule(that,validation){
	if(validation.length != 0){
		if(validation.rule){
			var rules = validation.rule;
			// that.setAttribute("data-rule",rules);
		}
		if(validation['error-message']){
			var msg = validation['error-message'];
			that.setAttribute("data-error",msg);
		}
	}
}
function ValidateForm1(that){
	// unset the error variable
	error = [];
	var id = $(that).attr('id');
	var values = [];
	// count the number ogf groups 
	count = originalForm.fields.length;
	// intialize groups in array
	for(var j=0; j<count;j++){
		values[j]=[];
	}
	var lists = $('#'+id+' input,#'+id+' select,#'+id+' textarea');
	for(var i=0; i<lists.length;i++){
		elem =lists[i];
		var grp = $(elem).attr('data-group');
		var item = $(elem).attr('data-item');
		var val ='';
		if($(elem).is(':checkbox') || $(elem).is(':radio')){
			if($(elem).is(':checked')){
				val = $(elem).val();
			}
		}
		else{
			val = $(elem).val();
		}
		// loading answers to variable
		if(val || values[grp][item]){
			if(values[grp][item] && val){
				values[grp][item]=values[grp][item]+','+val;
			}
			else if(values[grp][item] && !val){
				values[grp][item]= values[grp][item];val.toString();
			}
			else{
				values[grp][item]= val.toString();
			}
		}
		else {
			if(val != null)	values[grp][item]=val.toString();
			else values[grp][item] = '';
		}
	}
	for(var k in values){
		if(values[k].length>0){
			for( var l in values[k]){
				/*
				*	check validation by passing 
				*	item, group position, item position,value
				*/
				originalForm.fields[k].items[l].value = values[k][l];
				validationRun(originalForm.fields[k].items[l],k,l,values[k][l]);
			}
		}
	}
	$('#output').html(JSON.stringify(originalForm,null,2));
	if(Object.keys(error).length>0){
		console.log(error);
		$('#output').html(JSON.stringify(error,null,2));
	}else{
		console.log(originalForm);
		$('#output').html(JSON.stringify(originalForm,null,2));
	}
		
	return false;
}
function validationRun(item,g_no,i_no){
	var rule = item.validation.rule;
	if(rule && Object.keys(rule).length > 0){
		for(var i in rule){
			var err = findValidate(i,item.value);
			if(err){ error.push(rule[i]);break;}
		}
	}
}
function findValidate(rule,val){
	switch(rule){
		
		case 'required':
		return validRequired(rule,val);
		break;
		
		case 'number':
		return validNumber(rule,val);
		break;
		
		case 'email':
		return validEmail(rule,val);
		break;
		
		default:
		return validDefault(rule,val);
		break;
	}
}
function validRequired(rule,val){
	if(!val.trim()){	return true;}else{return false;}
}
function validNumber(rule,val){
	if(isNaN(val)){ return true; }else{ return false; }
}
function validEmail(rule,val){
	var regexp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!val.match(regexp)){ return true; }else{ return false; }
}
function validDefault(rule,val){
	var rule1 = rule.split('custom_');
	return eval(rule1[1])(rule,val);
}
function number(rule,val){
		if(isNaN(val)){ return true; }else{ return false; }
}
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
function addElement(that){
	element = that.getAttribute('data-element');
	limit = that.getAttribute('data-limit');
	g_no = that.getAttribute('data-group');
	i_no = that.getAttribute('data-item');
	lbl = that.getAttribute('data-label');
	var col6 = document.createElement("div");
	col6.setAttribute("class","medium-6 column");
	var col4 = document.createElement("div");
	col4.setAttribute("class","medium-4 column");
	var col2 = document.createElement("div");
	col2.setAttribute("class","medium-2 column float-right");
	var form_group = document.createElement("div");
	form_group.setAttribute("class","form-group");
	var clear = document.createElement("div");
	clear.setAttribute("style","clear:both");
	var item = {label:lbl,value:"",validation:{},type:element};
	var input = findType(item,g_no,i_no);
	var nam = input.getAttribute("name");
	input.setAttribute("name",nam+'[]');
	rm_btn = removeButton(form_group);
	col6.appendChild(input);
	col4.innerHTML="&nbsp;";
	col2.appendChild(rm_btn);
	form_group.appendChild(clear);
	form_group.appendChild(col4);
	form_group.appendChild(col6);
	form_group.appendChild(col2);
	var parent = that.parentElement.parentElement.parentElement;
	parent.appendChild(form_group);
}
function removeButton(form_group){
	button =  document.createElement("button");
	button.setAttribute("class","alert hollow button");
	button.setAttribute("type","button");
	button.innerHTML = "remove";
	button.onclick = function(){ form_group.remove(); };
	return button;

}
function removeElement(input){

}