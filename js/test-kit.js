var testkitObj = testkitObj || {};
testkitObj.originalForm = {};
testkitObj.error = [];
testkitObj.tmp = (Math.random() / +new Date()).toString(36).replace(/[^a-z]+/g, '');
(function($, window) {

    testkitObj.collectInfo = {

        /*
         *@inputs are path of json file and tagId where to populate form
         * @returns json
         */
        buildForm: function(url, element, raw = false) {
            testkitObj.element = document.getElementById(element);
            var data = testkitObj.collectInfo.httpRequest(url, 'get', {}, raw);

        },
        /*
         * @input path or URL 
         * @return json
         */
        httpRequest: function(url, type, param, raw) {
            // to be initialized, if anyting
            $.ajax({
                type: type,
                url: url,
                data: param,
                beforeSend: function() {
                    //
                },
                complete: function() {
                    //
                },
                success: function(res) {
                    testkitObj.originalForm = res;
                    testkitObj.collectInfo.buildFormElements();
                    if (raw) document.getElementById(raw).innerHTML = JSON.stringify(res, null, 1);

                }
            });
        },
        /*
         * getting form ajax response
         */
        buildFormElements: function() {
            var data = testkitObj.originalForm;
            var elem = testkitObj.element;
            var title = data.title;
            var fields = data.fields;
            var h2 = document.createElement("h3");
            h2.innerHTML = title;
            elem.appendChild(h2);
            for (var i in fields) {
                var groupTag = document.createElement("div");
                groupTag.setAttribute("class", "group");
                var hTag = document.createElement("h4");
                var hText = document.createTextNode(fields[i]['group-title']);
                hTag.appendChild(hText);
                groupTag.appendChild(hTag);
                groupTag.appendChild(testkitObj.collectInfo.groupBlock(fields[i].items, i));
                elem.appendChild(groupTag);
            }

        },
        /*
         * to create the grouping of every subheading ogf the form
         */
        groupBlock: function(items, g_no) {
            var fieldset = document.createElement('div');
            fieldset.setAttribute('class', 'formset');
            for (var j in items) {
                testkitObj.tmp = (Math.random() / +new Date()).toString(36).replace(/[^a-z]+/g, '');
                var row = document.createElement('div');
                row.setAttribute('class', 'row');
                var form_group = document.createElement('div');
                form_group.setAttribute("class", "form-group");
                var col4 = document.createElement("div");
                col4.setAttribute("class", "medium-4 columns text-right");
                var col8_ = document.createElement("div");
                var col2 = document.createElement("div");
                col2.setAttribute("class", "medium-2 columns float-right");
                var helpText = document.createElement("p");
                helpText.setAttribute("class", "help-text");
                var label = document.createElement('label');
                var br = document.createElement("br");
                var lblText = document.createTextNode(items[j].label);
                labelName = items[j].label;
                helpText.setAttribute("id", labelName.replace(" ", "_").toLowerCase());
                label.appendChild(lblText);
                // call findType for type of elememnt required
                var inputs = testkitObj.collectInfo.findType(items[j], g_no, j);
                var isRequire = items[j].validation.rule;
                if (isRequire.required) {
                    var span = document.createElement("span");
                    span.setAttribute("class", "error");
                    var require = document.createTextNode('*');
                    span.appendChild(require);
                    label.appendChild(span);
                }

                col4.appendChild(label);

                /* if the item has add more option  */
                col8_.appendChild(inputs);
                if (items[j].addmore) {
                    if (items[j].addmore.button) {
                        var btn = testkitObj.collectInfo.addButton(items[j].addmore.button, items[j].type, 5, g_no, j); // addButton(label,element,limit)
                        col2.appendChild(btn);
                        form_group.appendChild(col2);
                    }
                    var nam = inputs.getAttribute("name");
                    if (nam != null) {
                        inputs.setAttribute("name", nam + '[]');
                    }
                    col8_.setAttribute("class", "medium-6 columns");
                } else {
                    col8_.setAttribute("class", "medium-8 columns");
                }
                /* end add more option */
                /* extra inputs */
                var extra = false;
                extra = items[j].extra;
                if (extra) {
                    col8_.appendChild(testkitObj.collectInfo.typeExtraText(extra, g_no, j, '', ''));
                }
                /* end extra input */
                form_group.appendChild(col4);
                form_group.appendChild(col8_);
                form_group.appendChild(helpText);
                row.appendChild(form_group);
                fieldset.appendChild(row);
                fieldset.appendChild(br);

            }
            return fieldset;
        },

        findType: function(types, g_no, i_no, style = "") {
            var type = types.type;
            switch (type) {
                case 'text':
                    // 
                    return testkitObj.collectInfo.typeText(types, g_no, i_no, '', style);
                    break;
                case 'date':
                    return testkitObj.collectInfo.typeText(types, g_no, i_no, 'date');
                    break;
                case 'select':
                    return testkitObj.collectInfo.typeSelect(types, g_no, i_no);
                    break;
                case 'multiselect':
                    return testkitObj.collectInfo.typeMultiselect(types, g_no, i_no);
                    break;
                case 'checkbox':
                    return testkitObj.collectInfo.typeCheckbox(types, g_no, i_no);
                    // return typeRadio(types);
                    break;
                case 'option':
                    return testkitObj.collectInfo.typeRadio(types, g_no, i_no);
                    break;
                case 'upload':
                    return testkitObj.collectInfo.typeUpload(types, g_no, i_no);
                    break;
                case 'file':
                    return testkitObj.collectInfo.typeUpload(types, g_no, i_no);
                    break;
                case 'multientity':
                    return testkitObj.collectInfo.typeMultiEntity(types, g_no, i_no)
                    break;
                case 'textarea':
                    return testkitObj.collectInfo.typeTextarea(types, g_no, i_no, '', style);
                    // return typeText(types);
                    break;
            }
        },
        createExtraDiv: function(text, input) {
            var div = document.createElement("div");
            var clearfix = document.createElement("div");
            clearfix.setAttribute("class", "clearfix");
            div.setAttribute("class", "col-md-12");
            var label = document.createElement("label");
            var textNode = document.createTextNode(text);
            var newId = "randomid_" + (Math.random() / +new Date()).toString(36).replace(/[^a-z]+/g, '');
            label.setAttribute("for", newId);
            label.setAttribute("class", "col-md-6");
            input.setAttribute("id", newId);
            input.setAttribute("class", "col-md-6");
            div.appendChild(clearfix);
            label.appendChild(textNode);
            div.appendChild(label);
            div.appendChild(input);
            return div;

        },
        typeExtraText: function(formTag, g_no, i_no, date = null, style = "") {
            var lbl = formTag.label;
            var name = lbl.replace(" ", "_").toLowerCase();
            var input = document.createElement('input');
            var related = formTag.related;
            var text = formTag.hasOwnProperty("text") ? formTag.text : null;
            var placeholder = formTag.hasOwnProperty("placeholder") ? formTag.placeholder : null;
            var div = document.createElement("div");
            input.setAttribute("name", name);
            input.setAttribute("type", "text");
            input.setAttribute("data-group", g_no);
            input.setAttribute("data-item", i_no);
            input.setAttribute("data-related", related);
            input.setAttribute('disabled', true);
            if (date) input.setAttribute("class", "dt-picker");
            if (placeholder) input.setAttribute("placeholder", placeholder);
            if (placeholder) input.setAttribute("data-placeholder", placeholder);
            if (text) return div.appendChild(testkitObj.collectInfo.createExtraDiv(text, input));
            return div.appendChild(input);
        },

        typeText: function(formTag, g_no, i_no, date = null, style = "") {

            var lbl = formTag.label;
            var name = lbl.replace(" ", "_").toLowerCase();
            var value = formTag.value;
            var input = document.createElement('input');
            var related = formTag.related;
            var placeholder = formTag.hasOwnProperty("placeholder") ? formTag.placeholder : null;
            input.setAttribute("name", name);
            input.setAttribute("type", "text");
            input.setAttribute("data-group", g_no);
            input.setAttribute("data-item", i_no);
            if (value) input.setAttribute("value", value);
            if (date) input.setAttribute("class", "dt-picker");
            if (placeholder) input.setAttribute("placeholder", placeholder);
            if (placeholder) input.setAttribute("data-placeholder", placeholder);
            if (related && related.length) return input;
            testkitObj.collectInfo.validationRule(input, formTag.validation);
            return input;
        },
        typeSelect: function(formTag, g_no, i_no) {
            var lbl = formTag.label;
            var property = formTag.properties;
            var name = lbl.replace(" ", "_").toLowerCase();
            var value = formTag.value;
            var select = document.createElement('select');
            select.setAttribute("name", name);
            select.setAttribute("data-group", g_no);
            select.setAttribute("data-item", i_no);
            if (property) {
                for (var j in property) {
                    var option = document.createElement('option');
                    if (value == j) option.setAttribute("selected", true);
                    option.value = j;
                    option.innerHTML = property[j];
                    select.appendChild(option);
                }
            }
            return select;
        },
        typeMultiselect: function(formTag, g_no, i_no) {
            var lbl = formTag.label;
            var property = formTag.properties;
            var name = lbl.replace(" ", "_").toLowerCase();
            var value = formTag.value;
            value = value.split(',');
            var select = document.createElement('select');
            select.setAttribute("name", name + '[]');
            select.setAttribute("multiple", true);
            select.setAttribute("data-group", g_no);
            select.setAttribute("data-item", i_no);
            if (property) {
                for (var j in property) {
                    var option = document.createElement('option');
                    if (testkitObj.inArray(j, value)) option.setAttribute("selected", true);
                    option.value = j;
                    option.innerHTML = property[j];
                    select.appendChild(option);
                }
            }
            return select;
        },
        isExtra: function(e) {
            var that = this;
            var current_value = that.getAttribute('value');
            var current_name = that.getAttribute("name");
            var elem = document.querySelectorAll("input[name='" + current_name + "'][data-related='" + current_value + "']");

            if (that.checked) {
                if (elem.length) {
                    elem[0].removeAttribute("disabled");
                    elem[0].focus();
                }
            } else {
                if (elem.length) {
                    elem[0].value = "";
                    elem[0].setAttribute("disabled", true);
                }
            }
        },
        typeCheckbox: function(formTag, g_no, i_no) {
            var lbl = formTag.label;
            var name = lbl.replace(" ", "_").toLowerCase();
            var value = formTag.value;
            value = value.split(",");
            var div = document.createElement('div');
            var property = formTag.properties;
            var i = 0;
            if (property) {
                for (var k in property) {
                    var tmp_id = testkitObj.tmp + i;
                    var div_ = document.createElement("div");
                    div_.setAttribute("class", "medium-6 column");
                    var input = document.createElement("input");
                    var optText = document.createTextNode(property[k]);
                    var label = document.createElement("label");
                    testkitObj.collectInfo.validationRule(input, formTag.validation);
                    input.setAttribute("name", name + '[]');
                    input.setAttribute("value", k);
                    input.setAttribute("data-group", g_no);
                    input.setAttribute("data-item", i_no);
                    input.onchange = testkitObj.collectInfo.isExtra;
                    // check whether value checked or not
                    if (testkitObj.inArray(k, value)) input.setAttribute("checked", true);
                    input.setAttribute("id", tmp_id);
                    input.setAttribute("type", "checkbox");
                    label.setAttribute("for", tmp_id);
                    label.appendChild(optText);
                    div_.appendChild(input);
                    div_.appendChild(label);
                    div.appendChild(div_);
                    i++;
                }
            }
            return div;
        },
        typeRadio: function(formTag, g_no, i_no) {
            var lbl = formTag.label;
            var name = lbl.replace(" ", "_").toLowerCase();
            var value = formTag.value;
            var div = document.createElement('div');
            var property = formTag.properties;
            if (property) {
                for (var k in property) {
                    var div_ = document.createElement("div");
                    div_.setAttribute("class", "medium-6 column");
                    var input = document.createElement("input");
                    var optText = document.createTextNode(property[k]);
                    var label = document.createElement("label");
                    testkitObj.collectInfo.validationRule(input, formTag.validation);
                    input.setAttribute("name", name);
                    input.setAttribute("value", k);
                    input.setAttribute("id", k);
                    input.setAttribute("data-group", g_no);
                    input.setAttribute("data-item", i_no);
                    if (k == value) input.setAttribute("checked", true);
                    input.setAttribute("type", "radio");
                    label.setAttribute("for", k);
                    label.appendChild(optText);
                    div_.appendChild(input);
                    div_.appendChild(label);
                    div.appendChild(div_);
                }
            }
            return div;
        },
        typeUpload: function(formTag, g_no, i_no) {
            var lbl = formTag.label;
            var name = lbl.replace(" ", "_").toLowerCase();
            var value = formTag.value;
            var file = document.createElement('input');
            var div = document.createElement("div");
            var label = document.createElement("label");
            label.setAttribute("class", "hollow button");
            label.setAttribute("for", lbl);
            label.innerHTML = "Upload";
            file.setAttribute("name", name);
            file.setAttribute("type", 'file');
            file.setAttribute("data-group", g_no);
            file.setAttribute("data-item", i_no);
            file.setAttribute("class", "show-for-sr");
            file.setAttribute("id", lbl);
            testkitObj.collectInfo.validationRule(file, formTag.validation);
            div.appendChild(label);
            div.appendChild(file);
            return div;
        },
        typeTextarea: function(formTag, g_no, i_no, date = null, style = "") {
            var lbl = formTag.label;
            var name = lbl.replace(" ", "_").toLowerCase();
            var value = formTag.value;
            var related = formTag.related;
            var textarea = document.createElement('textarea');
            textarea.setAttribute("name", name);
            textarea.setAttribute("value", value);
            textarea.setAttribute("cols", 3);
            textarea.setAttribute("rows", 3);
            textarea.setAttribute("data-group", g_no);
            textarea.setAttribute("data-item", i_no);

            if (value) textarea.setAttribute("value", value);
            if (related && related.length) return textarea;
            textarea.innerHTML = value;
            testkitObj.collectInfo.validationRule(textarea, formTag.validation);
            return textarea;
        },
        validationRule: function(that, validation) {
            if (validation.length != 0) {
                if (validation.rule) {
                    var rules = validation.rule;
                    // that.setAttribute("data-rule",rules);
                }
                if (validation['error-message']) {
                    var msg = validation['error-message'];
                    that.setAttribute("data-error", msg);
                }
            }
        },
        addButton: function(label, element, limit, g_no, i_no) {
            var btn = document.createElement("button");
            btn.setAttribute("type", "button");
            btn.setAttribute("onclick", "testkitObj.collectInfo.addElement(this)");
            btn.setAttribute("data-limit", limit);
            btn.setAttribute("data-group", g_no);
            btn.setAttribute("data-item", i_no);
            btn.setAttribute("data-element", element);
            btn.setAttribute("data-label", label);
            btn.setAttribute("class", "hollow button");
            btn.innerHTML = label;
            return btn;
        },
        addElement: function(that) {
            element = that.getAttribute('data-element');
            limit = that.getAttribute('data-limit');
            g_no = that.getAttribute('data-group');
            i_no = that.getAttribute('data-item');
            lbl = that.getAttribute('data-label');
            var col6 = document.createElement("div");
            col6.setAttribute("class", "medium-6 column");
            var col4 = document.createElement("div");
            col4.setAttribute("class", "medium-4 column");
            var col2 = document.createElement("div");
            col2.setAttribute("class", "medium-2 column float-right");
            var form_group = document.createElement("div");
            form_group.setAttribute("class", "form-group");
            var clear = document.createElement("div");
            clear.setAttribute("style", "clear:both");
            var item = { label: lbl, value: "", validation: {}, type: element };
            var input = testkitObj.collectInfo.findType(item, g_no, i_no);
            var nam = input.getAttribute("name");
            input.setAttribute("name", nam + '[]');
            rm_btn = testkitObj.collectInfo.removeButton(form_group);
            col6.appendChild(input);
            col4.innerHTML = "&nbsp;";
            col2.appendChild(rm_btn);
            form_group.appendChild(clear);
            form_group.appendChild(col4);
            form_group.appendChild(col6);
            form_group.appendChild(col2);
            var parent = that.parentElement.parentElement.parentElement;
            parent.appendChild(form_group);
        },
        removeButton: function(form_group) {
            button = document.createElement("button");
            button.setAttribute("class", "alert hollow button");
            button.setAttribute("type", "button");
            button.innerHTML = "remove";
            button.onclick = function() { form_group.remove(); };
            return button;
        }
    };
    testkitObj.inArray = function(needle, haystack) {
        var length = haystack.length;
        for (var i = 0; i < length; i++) {
            if (haystack[i] == needle) return true;
        }
        return false;
    };
    testkitObj.formSubmit = function(that) {
        testkitObj.error = [];
        var id = $(that).attr('id');
        var values = [];
        // count the number ogf groups 
        count = testkitObj.originalForm.fields.length;
        // intialize groups in array
        for (var j = 0; j < count; j++) {
            values[j] = [];
        }
        var lists = $('#' + id + ' input,#' + id + ' select,#' + id + ' textarea');
        for (var i = 0; i < lists.length; i++) {
            elem = lists[i];
            var grp = $(elem).attr('data-group');
            var item = $(elem).attr('data-item');
            var val = '';
            if ($(elem).is(':checkbox') || $(elem).is(':radio')) {
                if ($(elem).is(':checked')) {
                    val = $(elem).val();
                }
            } else {
                val = $(elem).val();
            }
            // loading answers to variable
            if (val || values[grp][item]) {
                if (values[grp][item] && val) {
                    values[grp][item] = values[grp][item] + ',' + val;
                } else if (values[grp][item] && !val) {
                    values[grp][item] = values[grp][item];
                    val.toString();
                } else {
                    values[grp][item] = val.toString();
                }
            } else {
                if (val != null) values[grp][item] = val.toString();
                else values[grp][item] = '';
            }
        }
        for (var k in values) {


            if (values[k].length > 0) {
                for (var l in values[k]) {
                    /*
                     *	check validation by passing 
                     *	item, group position, item position,value
                     */
                    testkitObj.originalForm.fields[k].items[l].value = values[k][l];
                    testkitObj.validation.run(testkitObj.originalForm.fields[k].items[l], k, l, values[k][l]);
                    testkitObj.checkExtraField(testkitObj.originalForm.fields[k].items[l], k, l, values[k][l]);
                }
            }

        }
        $('#output').html(JSON.stringify(testkitObj.originalForm, null, 2));
        if (Object.keys(testkitObj.error).length > 0) {
            $('#output').html(JSON.stringify(testkitObj.error, null, 2));
        } else {
            $('#output').html(JSON.stringify(testkitObj.originalForm, null, 2));
        }
        return false;
    };
    // here getting all the submited answers with comma seperate
    // and this is mainly for searching others check box if it is then checking textbox or textarea

    testkitObj.checkExtraField = function(list, group, item, value) {
        var values = list.value;
        values = values.split(',');
        if (values.length > 0) {
            for (var n in values) {
                var name = values[n];
                var fieldName = list.label;
                fieldName = fieldName.replace(" ", "_");
                fieldName = fieldName.toLowerCase();
                var elem = document.querySelectorAll("input[name^='" + fieldName + "'][data-related='" + name + "']");

                if (elem.length > 0) {
                    if (!elem[0].value) {
                        testkitObj.error.push(list.extra.required);
                    }

                }
            }
        }

    };

    testkitObj.validation = {
        run: function(item, g_no, i_no) {
            var rule = item.validation.rule;
            if (rule && Object.keys(rule).length > 0) {
                for (var i in rule) {
                    var err = testkitObj.validation.findValidate(i, item.value);
                    if (err) { testkitObj.error.push(rule[i]); break; }
                }
            }
        },
        findValidate: function(rule, val) {
            switch (rule) {
                case 'required':
                    return testkitObj.validation.validRequired(rule, val);
                    break;

                case 'number':
                    return testkitObj.validation.validNumber(rule, val);
                    break;

                case 'email':
                    return testkitObj.validation.validEmail(rule, val);
                    break;

                default:
                    return testkitObj.validation.validDefault(rule, val);
                    break;
            }
        },
        validRequired: function(rule, val) {
            if (!val.trim()) { return true; } else { return false; }
        },
        validNumber: function(rule, val) {
            if (isNaN(val)) { return true; } else { return false; }
        },
        validEmail: function(rule, val) {
            var regexp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!val.match(regexp)) { return true; } else { return false; }
        },
        validDefault: function(rule, val) {
            var rule1 = rule.split('custom_');
            return eval(rule1[1])(rule, val);
        }
    };

})(jQuery, window);