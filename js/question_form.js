var originalForm = '';
var error = {};
/* Object.key(obj).length for count length of Object array for manul*/
if (!Object.keys) {
    Object.keys = function (obj) {
        var arr = [],
            key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                arr.push(key);
            }
        }
        return arr;
    };
}
/* End length of object array */
function buildForm(data,element){
	var elem = document.getElementById(element);
	var title = data.title;
	var fields = data.fields;
	for(var i in fields){
		var groupTag = document.createElement("div");
			groupTag.setAttribute("class","group");
		var hTag = document.createElement("h4");
		var hText = document.createTextNode(fields[i]['group-title']);
			hTag.appendChild(hText);
		groupTag.appendChild(hTag);
		groupTag.appendChild(groupBlock(fields[i].items,i));
		elem.appendChild(groupTag);
	}
}
function groupBlock(items,g_no) {
		var fieldset = document.createElement('div');
			fieldset.setAttribute('class','');
	for(var j in items){
		var row = document.createElement('div');
			row.setAttribute('class','row');
		var col12 = document.createElement("div");
			col12.setAttribute("class","medium-12 columns");
		var col8 = document.createElement("div");
			col8.setAttribute("class","medium-8 columns");
		var helpText = document.createElement("p");
			helpText.setAttribute("class","help-text");
		var label = document.createElement('label');
		var lblText = document.createTextNode(items[j].name+'. '+items[j].label);
			labelName = items[j].label;
			helpText.setAttribute("id",labelName.replace(" ","_").toLowerCase());
			label.appendChild(lblText);
			var inputs = findType(items[j],g_no,j);
			var isRequire = items[j].validation.rule;
			if(isRequire.required){ var require = document.createTextNode('(*)');label.appendChild(require); }
			label.appendChild(inputs);
			col12.appendChild(label);
			// console.log(inputs);
			// return;
		// col8.appendChild(inputs);
		// row.appendChild(col4);
		row.appendChild(col12);
		row.appendChild(helpText);
		fieldset.appendChild(row);
	}
	return fieldset;
}
function findType(types,g_no,i_no){
	var type = types.type;
	switch(type){
		case 'text':
			// 
			return typeText(types,g_no,i_no);
			break;
		case 'select':
			return typeSelect(types,g_no,i_no);
			break;
		case 'multiselect':
			return typeMultiselect(types,g_no,i_no);
			break;
		case 'checkbox':
			return typeCheckbox(types,g_no,i_no);
			// return typeRadio(types);
			break;
		case 'option':
			return typeRadio(types,g_no,i_no);
			break;
		case 'upload':
			return typeUpload(types,g_no,i_no);
			break;
		case 'textarea':
			return typeTextarea(types,g_no,i_no);
			// return typeText(types);
			break;
	}
}
function typeText(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = formTag.name;
	var value = formTag.value;
	var input = document.createElement('input');
	input.setAttribute("name",name);
	input.setAttribute("value",value);
	input.setAttribute("type","text");
	input.setAttribute("data-group",g_no);
	input.setAttribute("data-item",i_no);
	validationRule(input,formTag.validation);
	return input;
}
function typeTextarea(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = formTag.name;
	var value = formTag.value;
	var textarea = document.createElement('textarea');
	textarea.setAttribute("name",name);
	textarea.setAttribute("value",value);
	textarea.setAttribute("cols",3);
	textarea.setAttribute("rows",3);
	textarea.setAttribute("data-group",g_no);
	textarea.setAttribute("data-item",i_no);
	textarea.innerHTML=value;
	validationRule(textarea,formTag.validation);
	return textarea;
}
function typeSelect(formTag,g_no,i_no){

	var lbl = formTag.label;
	var property = formTag.properties;
	var name = formTag.name;
	var value = formTag.value;
	var select = document.createElement('select');
		select.setAttribute("name",name);
		select.setAttribute("data-group",g_no);
		select.setAttribute("data-item",i_no);
	if(property){
		for(var j in property){
			var option = document.createElement('option');
			if(value == j) option.setAttribute("selected",true);
			option.value = j;
			option.innerHTML = property[j]; 
			select.appendChild(option);
		}
	}
	return select;
}

function typeMultiselect(formTag,g_no,i_no){
	var lbl = formTag.label;
	var property = formTag.properties;
	var name = formTag.name;
	var value = formTag.value;
	var select = document.createElement('select');
		select.setAttribute("name",name+'[]');
		select.setAttribute("multiple",true);
		select.setAttribute("data-group",g_no);
		select.setAttribute("data-item",i_no);
	if(property){
		for(var j in property){
			var option = document.createElement('option');
			if(value == j) option.setAttribute("selected",true);
			option.value = j;
			option.innerHTML = property[j]; 
			select.appendChild(option);
		}
	}
	return select;
}

function typeCheckbox(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = formTag.name;
	var value = formTag.value;
		value = value.split(',');
	var div = document.createElement('div');
	var property = formTag.properties;
	if(property){
		for(var k in property){
			var col6 = document.createElement("div");
			col6.setAttribute("class","medium-6 column");
			var input = document.createElement("input");
			var optText = document.createTextNode(property[k]);
			var label = document.createElement("label");
			validationRule(input,formTag.validation);
			input.setAttribute("name",name+'[]');
			input.setAttribute("value",k);
			input.setAttribute("data-group",g_no);
			input.setAttribute("data-item",i_no);
			// check if the value is multiple or single
			if(inArray(k,value)) input.setAttribute("checked",true);
			input.setAttribute("id",k);
			input.setAttribute("type","checkbox");
			label.setAttribute("for",k);
			label.appendChild(optText);
			col6.appendChild(input);
			col6.appendChild(label);
			div.appendChild(col6);
		}
	}
	return div;
}
function typeRadio(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = formTag.name;
	var value = formTag.value;
	var div = document.createElement('div');
	var property = formTag.properties;
	if(property){
		for(var k in property){
			var col6 = document.createElement("div");
			col6.setAttribute("class","medium-6 column");
			var input = document.createElement("input");
			var optText = document.createTextNode(property[k]);
			var label = document.createElement("label");
			validationRule(input,formTag.validation);
			input.setAttribute("name",name);
			input.setAttribute("value",k);
			input.setAttribute("id",k);
			input.setAttribute("data-group",g_no);
			input.setAttribute("data-item",i_no);
			if(k == value) input.setAttribute("checked",true);
			input.setAttribute("type","radio");
			label.setAttribute("for",k);
			label.appendChild(optText);
			col6.appendChild(input);
			col6.appendChild(label);
			div.appendChild(col6)
		}
	}
	return div;
}

function typeUpload(formTag,g_no,i_no){
	var lbl = formTag.label;
	var name = formTag.name;
	var value = formTag.value;
	var file = document.createElement('input');
	file.setAttribute("name",name);
	file.setAttribute("type",'file');
	file.setAttribute("data-group",g_no);
	file.setAttribute("data-item",i_no);
	validationRule(file,formTag.validation);
	return file;
}
function validationRule(that,validation){
	if(validation.length != 0){
		if(validation.rule){
			var rules = validation.rule;
			that.setAttribute("data-rule",rules);
		}
		if(validation['error-message']){
			var msg = validation['error-message'];
			that.setAttribute("data-error",msg);
		}
	}
}
function ValidateForm1(that){
	// unset the error variable
	error = {};
	var id = $(that).attr('id');
	var values = [];
	// count the number ogf groups 
	count = originalForm.fields.length;
	// intialize groups in array
	for(var j=0; j<count;j++){
		values[j]=[];
	}
	var lists = $('#'+id+' input,#'+id+' select,#'+id+' textarea');
	for(var i=0; i<lists.length;i++){
		elem =lists[i];
		var grp = $(elem).attr('data-group');
		var item = $(elem).attr('data-item');
		var val ='';
		if($(elem).is(':checkbox') || $(elem).is(':radio')){
			if($(elem).is(':checked')){
				val = $(elem).val();
			}
		}
		else{
			val = $(elem).val();
		}
		// loading answers to variable
		if(val || values[grp][item]){
			if(values[grp][item] && val){
				values[grp][item]=values[grp][item]+','+val;
			}
			else if(values[grp][item] && !val){
				values[grp][item]= values[grp][item];val.toString();
			}
			else{
				values[grp][item]= val.toString();
			}
		}
		else {
			if(val != null)	values[grp][item]=val.toString();
			else values[grp][item] = '';
		}
	}
	for(var k in values){
		if(values[k].length>0){
			for( var l in values[k]){
				/*
				*	check validation by passing 
				*	item, group position, item position,value
				*/
				originalForm.fields[k].items[l].value = values[k][l];
				validationRun(originalForm.fields[k].items[l],k,l,values[k][l]);
			}
		}
	}
	$('#output').html(JSON.stringify(originalForm,null,2));
	if(Object.keys(error).length>0){
		console.log(error);
		$('#output').html(JSON.stringify(error,null,2));
	}else{
		console.log(originalForm);
		$('#output').html(JSON.stringify(originalForm,null,2));
	}
		
	return false;
}
function validationRun(item,g_no,i_no){
	var rule = item.validation.rule;
	if(rule && Object.keys(rule).length > 0){
		for(var i in rule){
			var err = findValidate(i,item.value);
			if(err){ error[item.name] = rule[i];break;}
		}
	}
}
function findValidate(rule,val){
	switch(rule){
		
		case 'required':
		return validRequired(rule,val);
		break;
		
		case 'number':
		return validNumber(rule,val);
		break;
		
		case 'email':
		return validEmail(rule,val);
		break;
		
		default:
		return validDefault(rule,val);
		break;
	}
}
function validRequired(rule,val){
	if(!val.trim()){	return true;}else{return false;}
}
function validNumber(rule,val){
	if(isNaN(val)){ return true; }else{ return false; }
}
function validEmail(rule,val){
	var regexp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!val.match(regexp)){ return true; }else{ return false; }
}
function validDefault(rule,val){
	var rule1 = rule.split('custom_');
	return eval(rule1[1])(rule,val);
}
function number(rule,val){
		if(isNaN(val)){ return true; }else{ return false; }
}
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}