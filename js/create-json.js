function formData(){
	var item = $('#createjson .form-group-title');
	var data = {};
	data.title = "";
	data.fields=[];
		$.each(item,function(k,v){
			var group_title = $(v).children('.group_title');
			var title = $(v).children('.title');
			var items = $(v).children('.form-group');
			if( group_title.length !=0){ var group_title = group_title[0].value;}
			if( title.length !=0){ var title = title[0].value;}
			var item_count = items.length;
			data.fields.push({'group-title':group_title,items:[]});
			// console.log(data);
			// return false;
			for(var i=0; i<item_count; i++){
				// if(k==1){ console.log(data); return false;}
				var label = $(items[i]).children('.label')[0].value;
				var type = $(items[i]).children('.type')[0].value;
				var val = $(items[i]).children('.value')[0].value;
				var defaulte = $(items[i]).children('.default')[0].value;
				var note = $(items[i]).children('.note')[0].value;
				var validation = $($(items[i]).children('.validation_cls')).children('.validation');
				var msg = $($(items[i]).children('.validation_cls')).children('.valid_message');
				var properties = $($(items[i]).children('.properties_cls')).children('.properties');
				var pro ={};
				$.each(properties,function(l,m){
					if($(m).val() != ''){
						pro[$(m).val()]=$(m).val();
					}
				});
				var valid = {}
				var c=0;
				$.each(validation,function(n,o){
					if($(msg[c]).val()!=''){
						valid[$(o).val()]=$(msg[c]).val();
						
					}
					c++;
				});
				data.fields[k].items.push({
					"label":label,
					"type":type,
					"validation":{"rule":valid},
					"properties":pro,
					"value":val,
					"default":defaulte,
					"note":note
				});
			}
			
		});
		$("#json").html(JSON.stringify(data,null,2));

		return false;
	}
function addVaild(that){
	var select = document.createElement("select");
	select.setAttribute("class","validation");
	var ops = ["required","email","number"];
	for(var i in ops){
		var option = document.createElement("option");
		option.setAttribute("value",ops[i]);
		option.innerHTML = ops[i];
		select.appendChild(option);
	}
	var input = document.createElement("input");
	input.setAttribute("class", 'valid_message');
	var remove = document.createElement("a");
	remove.setAttribute("href","javascript://");
	remove.setAttribute("onclick","removeValid(this)");
	remove.innerHTML="Remove";
	var br = document.createElement("br");
	var main = $(that).parent()[0];
	main.appendChild(select);
	main.appendChild(input);
	main.appendChild(remove);
	main.appendChild(br);
}
function removeValid(that){
	$(that).prev().remove();
	$(that).prev().remove();
	$(that).next().remove();
	$(that).remove();
}
function addProp(that){
	var input = document.createElement("input");
	var br = document.createElement("br");
	var remove = document.createElement("a");
	input.setAttribute("class","properties");
	remove.setAttribute("href","javascript://");
	remove.setAttribute("onclick","removeProp(this)");
	remove.innerHTML = "Remove";
	var parent = $(that).parent()[0];
	parent.appendChild(input);
	parent.appendChild(remove);
	parent.appendChild(br);
}
function removeProp(that){
	$(that).next().remove();
	$(that).prev().remove();
	$(that).remove();
}
function addGroup(that){
	var group = document.createElement("div");
	group.setAttribute("class","form-group-title");
	var formGroup = document.createElement("div");
	formGroup.setAttribute("class","form-group");
	formGroup.setAttribute("style","border: 1px solid black");
	var text= document.createTextNode("Group Title :");
	var input = document.createElement("input");
	input.setAttribute("class","group_title");
	var addItem = document.createElement("a");
	addItem.setAttribute("href", "javascript://");
	addItem.setAttribute("onclick","addItem(this)");
	addItem.innerHTML = "Add Item";
	var space = document.createElement("div");
	space.appendChild(document.createTextNode("|"))
	/* Remove*/

	var removeGroup = document.createElement("a");
	removeGroup.setAttribute("href", "javascript://");
	removeGroup.setAttribute("onclick", "removeGroup(this)");
	removeGroup.innerHTML = "Remove Group";

	group.appendChild(addItem);
	group.appendChild(removeGroup);
	group.appendChild(text);
	group.appendChild(input);
	// group.appendChild(formGroup);
	document.getElementById("groups").appendChild(group);
}
function removeGroup(that){
	$(that).parent().parent().remove();
}
function removeItem(that){
	$(that).parent().remove();
}
function addItem(that){
	var parent = $(that).parent()[0];
	var hr = document.createElement("hr");
	var div = document.createElement("div");
	var rm = document.createElement("a");
	rm.setAttribute("href", "javascript://");
	rm.setAttribute("onclick", "removeItem(this)");
	rm.innerHTML = "Remove Items";
	div.setAttribute("class","form-group");
	div.setAttribute("style","border: 1px solid black");
	var lblLabel=document.createTextNode("label");
	// div.appendChild(hr);
	div.appendChild(rm);
	div.appendChild(lblLabel);
	var inputLabel = document.createElement("input");
	inputLabel.setAttribute("class","label");
	inputLabel.setAttribute("type","text");
	var brLabel = document.createElement("br");
	div.appendChild(inputLabel);
	div.appendChild(brLabel);
	/* type */

	var lblType=document.createTextNode("type");
	div.appendChild(lblType);
	var inputType = document.createElement("select");
	inputType.setAttribute("class","type");
	inputType.setAttribute("type","select");
	var opts = {"text":"text","textarea":"textarea","select":"select","option":"option","checkbox":"checkbox","file":"file"}
	for(var i in opts){
		var option = document.createElement("option");
		option.setAttribute("value",i);
		option.innerHTML = opts[i];
		inputType.appendChild(option);
	}
	var brType = document.createElement("br");
	div.appendChild(inputType);
	div.appendChild(brType);
	/*validation*/

	var lblVali = document.createTextNode("validation:");
	div.appendChild(lblVali);
	var divVali = document.createElement("div");
	divVali.setAttribute("class", "validation_cls");
	var inputVali = document.createElement("select");
	inputVali.setAttribute("class","validation");
	inputVali.setAttribute("type","select");
	var opts = {"required":"required","number":"number","email":"email"}
	for(var i in opts){
		var optionVali = document.createElement("option");
		optionVali.setAttribute("value",i);
		optionVali.innerHTML = opts[i];
		inputVali.appendChild(optionVali);
	}
	divVali.appendChild(inputVali);
	var inputMsg = document.createElement("input");
	inputMsg.setAttribute("class","valid_message");
	divVali.appendChild(inputMsg);
	var addVali = document.createElement("a");
	addVali.setAttribute("href", "javascript://");
	addVali.setAttribute("class", "add_valid");
	addVali.setAttribute("onclick", "addVaild(this)");
	addVali.innerHTML = "Add more";
	var brVali = document.createElement("br");
	divVali.appendChild(addVali);
	divVali.appendChild(brVali);
	div.appendChild(divVali);
	/* Properties */

	var lblProp = document.createTextNode("properties :");
	div.appendChild(lblProp);
	var divProp = document.createElement("div");
	divProp.setAttribute("class", "properties_cls");
	var inputProp = document.createElement("input");
	inputProp.setAttribute("class","properties");
	var addProp = document.createElement("a");
	addProp.setAttribute("href", "javascript://");
	addProp.setAttribute("onclick", "addProp(this)");
	addProp.innerHTML = "Add more";
	var brProp = document.createElement("br");
	divProp.appendChild(inputProp);
	divProp.appendChild(addProp);
	divProp.appendChild(brProp);
	div.appendChild(divProp);
	/* value*/

	var lblValue = document.createTextNode("Value :");
	div.appendChild(lblValue);
	var inputValue = document.createElement("input");
	inputValue.setAttribute("class","value");
	var brValue = document.createElement("br");
	div.appendChild(lblValue);
	div.appendChild(inputValue);
	div.appendChild(brValue);
	/*defaul*/

	var lblDefault = document.createTextNode("Default :");
	div.appendChild(lblDefault);
	var inputDefault = document.createElement("input");
	inputDefault.setAttribute("class","default");
	var brDefault = document.createElement("br");
	div.appendChild(lblDefault);
	div.appendChild(inputDefault);
	div.appendChild(brDefault);

	/*Note*/

	var lblNote = document.createTextNode("Note :");
	div.appendChild(lblNote);
	var inputNote = document.createElement("input");
	inputNote.setAttribute("class","note");
	var brNote = document.createElement("br");
	div.appendChild(lblNote);
	div.appendChild(inputNote);
	div.appendChild(brNote);

	
	parent.appendChild(div);
}