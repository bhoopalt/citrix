<!DOCTYPE html>
<html>
<head>
	<title>Create Json</title>

</head>
<body>
<form id="createjson" onsubmit="return formData()">
	Title:<input type="text" name="title" class="title">	 <br>
	<a href="javascript://" onclick="addGroup(this)">Add Group</a>
	<div id="groups">
		<div class="form-group-title">
			Group Title: <input type="text" name="group_title" class="group_title">
			<div class="form-group" style="border: 1px solid black">
				label:<input type="text" name="label" class="label"><br>
				type : <select name="type[]" class="type">
					<option value="text">text</option>
					<option value="textarea">textarea</option>
					<option value="select">select</option>
					<option value="option">radio</option>
					<option value="option">option</option>
					<option value="checkbox">checkbox</option>
					<option value="file">file</option>
				</select><br>
				validation:
				<div class="validation_cls">
				<select name="validation[]" class="validation">
					<option value="required">required</option>
					<option value="number">number</option>
					<option value="email">email</option>
				</select>
				<input type="text" name="valid_message[]" class="valid_message">
				<a href="javascript://" class="add_valid" onclick="addVaild(this)">Add more</a>
				<br>
				</div>
				properties : 
				<div class="properties_cls">
					<input type="text" name="properties[]" class="properties">
					<a href="javascript://" onclick="addProp(this)">Add More</a>
					<br>
				</div>
				
					Value :
					<input type="text" name="value[]" class="value"><br>
			
					default :
					<input type="text" name="default[]" class="default"><br>
			
					note :
					<input type="text" name="note[]" class="note"><br>
				
			</div>
				<a href="javascript://" onclick="addItem(this)">Add Items</a>
		</div>
	</div>
	<button type="submit">Submit</button>
</form>
<pre>
	
<div id="json"></div>
</pre>
<script src="js/vendor/jquery.min.js"></script>
<script src="js/create-json.js"></script>
</body>
</html>